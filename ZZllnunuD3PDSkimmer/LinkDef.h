// A list of classes Root does not understand by default and have to
// incorporated into a dictionary.

#include <vector>

#ifdef __MAKECINT__

#pragma extra_include "vector";

// Don't need these - ROOT already has dictionaries for them
//#pragma link C++ class vector<bool>+;
//#pragma link C++ class vector<short>+;
//#pragma link C++ class vector<float>+;
//#pragma link C++ class vector<double>+;
//#pragma link C++ class vector<unsigned short>+;
//#pragma link C++ class vector<int>+;
//#pragma link C++ class vector<unsigned int>+;
//#pragma link C++ class vector<long>+;
//#pragma link C++ class vector<float>+;
#pragma link C++ class vector<vector<unsigned int> >+;
#pragma link C++ class vector<vector<int> >+;
#pragma link C++ class vector<vector<double> >+;
#pragma link C++ class vector<vector<float> >+;
#pragma link C++ class std::pair<std::string,std::string >+;

#endif
