
#include "ZZllnunuD3PDSkimmer/ZZ2l2nuSkimmer.h"

void ZZ2l2nuSkimmer::MakeMissingBrchs()
{
  
  if(!b_RunNumber) {
    outputTree->SetBranchAddress("RunNumber", &RunNumber);
  }
  
  if(!b_EventNumber) {
    outputTree->SetBranchAddress("EventNumber", &EventNumber);
  }
  
  if(!b_mc_channel_number) {
    outputTree->SetBranchAddress("mc_channel_number", &mc_channel_number);
  }
  
  if(!b_mc_event_number) {
    outputTree->SetBranchAddress("mc_event_number", &mc_event_number);
  }
  
  if(!b_mc_event_weight) {
    outputTree->SetBranchAddress("mc_event_weight", &mc_event_weight);
  }
  
  if(!b_bcid) {
    outputTree->SetBranchAddress("bcid", &bcid);
  }
  
  if(!b_lbn) {
    outputTree->SetBranchAddress("lbn", &lbn);
  }
  
  if(!b_coreFlags) {
    outputTree->SetBranchAddress("coreFlags", &coreFlags);
  }
  
  if(!b_larError) {
    outputTree->SetBranchAddress("larError", &larError);
  }
  
  if(!b_tileError) {
    outputTree->SetBranchAddress("tileError", &tileError);
  }
  
  if(!b_top_hfor_type) {
    outputTree->SetBranchAddress("top_hfor_type", &top_hfor_type);
  }
  
  if(!b_averageIntPerXing) {
    outputTree->SetBranchAddress("averageIntPerXing", &averageIntPerXing);
  }
  
  if(!b_mu_n) {
    outputTree->SetBranchAddress("mu_n", &mu_n);
  }
  
  if(!b_mu_pt) {
    outputTree->SetBranchAddress("mu_pt", &mu_pt);
  }
  
  if(!b_mu_eta) {
    outputTree->SetBranchAddress("mu_eta", &mu_eta);
  }
  
  if(!b_mu_phi) {
    outputTree->SetBranchAddress("mu_phi", &mu_phi);
  }
  
  if(!b_mu_isCombinedMuon) {
    outputTree->SetBranchAddress("mu_isCombinedMuon", &mu_isCombinedMuon);
  }
  
  if(!b_mu_isStandAloneMuon) {
    outputTree->SetBranchAddress("mu_isStandAloneMuon", &mu_isStandAloneMuon);
  }
  
  if(!b_mu_isSegmentTaggedMuon) {
    outputTree->SetBranchAddress("mu_isSegmentTaggedMuon", &mu_isSegmentTaggedMuon);
  }
  
  if(!b_mu_isCaloMuonId) {
    outputTree->SetBranchAddress("mu_isCaloMuonId", &mu_isCaloMuonId);
  }
  
  if(!b_mu_loose) {
    outputTree->SetBranchAddress("mu_loose", &mu_loose);
  }
  
  if(!b_mu_medium) {
    outputTree->SetBranchAddress("mu_medium", &mu_medium);
  }
  
  if(!b_EF_mu10_Upsimumu_tight_FS) {
    outputTree->SetBranchAddress("EF_mu10_Upsimumu_tight_FS", &EF_mu10_Upsimumu_tight_FS);
  }
  
  if(!b_mu_charge) {
    outputTree->SetBranchAddress("mu_charge", &mu_charge);
  }
  
  if(!b_mu_id_theta_exPV) {
    outputTree->SetBranchAddress("mu_id_theta_exPV", &mu_id_theta_exPV);
  }
  
  if(!b_mu_me_theta_exPV) {
    outputTree->SetBranchAddress("mu_me_theta_exPV", &mu_me_theta_exPV);
  }
  
  if(!b_mu_ms_theta) {
    outputTree->SetBranchAddress("mu_ms_theta", &mu_ms_theta);
  }
  
  if(!b_mu_id_qoverp_exPV) {
    outputTree->SetBranchAddress("mu_id_qoverp_exPV", &mu_id_qoverp_exPV);
  }
  
  if(!b_mu_me_qoverp_exPV) {
    outputTree->SetBranchAddress("mu_me_qoverp_exPV", &mu_me_qoverp_exPV);
  }
  
  if(!b_mu_ms_qoverp) {
    outputTree->SetBranchAddress("mu_ms_qoverp", &mu_ms_qoverp);
  }
  
  if(!b_mu_id_phi_exPV) {
    outputTree->SetBranchAddress("mu_id_phi_exPV", &mu_id_phi_exPV);
  }
  
  if(!b_mu_me_phi_exPV) {
    outputTree->SetBranchAddress("mu_me_phi_exPV", &mu_me_phi_exPV);
  }
  
  if(!b_mu_ms_phi) {
    outputTree->SetBranchAddress("mu_ms_phi", &mu_ms_phi);
  }
  
  if(!b_mu_trackd0pvunbiased) {
    outputTree->SetBranchAddress("mu_trackd0pvunbiased", &mu_trackd0pvunbiased);
  }
  
  if(!b_mu_trackz0pvunbiased) {
    outputTree->SetBranchAddress("mu_trackz0pvunbiased", &mu_trackz0pvunbiased);
  }
  
  if(!b_mu_tracksigd0pvunbiased) {
    outputTree->SetBranchAddress("mu_tracksigd0pvunbiased", &mu_tracksigd0pvunbiased);
  }
  
  if(!b_mu_tracksigz0pvunbiased) {
    outputTree->SetBranchAddress("mu_tracksigz0pvunbiased", &mu_tracksigz0pvunbiased);
  }
  
  if(!b_mu_expectBLayerHit) {
    outputTree->SetBranchAddress("mu_expectBLayerHit", &mu_expectBLayerHit);
  }
  
  if(!b_mu_nBLHits) {
    outputTree->SetBranchAddress("mu_nBLHits", &mu_nBLHits);
  }
  
  if(!b_mu_nPixHits) {
    outputTree->SetBranchAddress("mu_nPixHits", &mu_nPixHits);
  }
  
  if(!b_mu_nPixelDeadSensors) {
    outputTree->SetBranchAddress("mu_nPixelDeadSensors", &mu_nPixelDeadSensors);
  }
  
  if(!b_mu_nSCTHits) {
    outputTree->SetBranchAddress("mu_nSCTHits", &mu_nSCTHits);
  }
  
  if(!b_mu_nSCTDeadSensors) {
    outputTree->SetBranchAddress("mu_nSCTDeadSensors", &mu_nSCTDeadSensors);
  }
  
  if(!b_mu_nPixHoles) {
    outputTree->SetBranchAddress("mu_nPixHoles", &mu_nPixHoles);
  }
  
  if(!b_mu_nSCTHoles) {
    outputTree->SetBranchAddress("mu_nSCTHoles", &mu_nSCTHoles);
  }
  
  if(!b_mu_nTRTHits) {
    outputTree->SetBranchAddress("mu_nTRTHits", &mu_nTRTHits);
  }
  
  if(!b_mu_nTRTOutliers) {
    outputTree->SetBranchAddress("mu_nTRTOutliers", &mu_nTRTOutliers);
  }
  
  if(!b_mu_etcone20) {
    outputTree->SetBranchAddress("mu_etcone20", &mu_etcone20);
  }
  
  if(!b_mu_etcone30) {
    outputTree->SetBranchAddress("mu_etcone30", &mu_etcone30);
  }
  
  if(!b_mu_etcone40) {
    outputTree->SetBranchAddress("mu_etcone40", &mu_etcone40);
  }
  
  if(!b_mu_ptcone20) {
    outputTree->SetBranchAddress("mu_ptcone20", &mu_ptcone20);
  }
  
  if(!b_mu_ptcone30) {
    outputTree->SetBranchAddress("mu_ptcone30", &mu_ptcone30);
  }
  
  if(!b_mu_ptcone40) {
    outputTree->SetBranchAddress("mu_ptcone40", &mu_ptcone40);
  }
  
  if(!b_mu_EFMG_index) {
    outputTree->SetBranchAddress("mu_EFMG_index", &mu_EFMG_index);
  }
  
  if(!b_mu_isLowPtReconstructedMuon) {
    outputTree->SetBranchAddress("mu_isLowPtReconstructedMuon", &mu_isLowPtReconstructedMuon);
  }
  
  if(!b_mu_etCore) {
    outputTree->SetBranchAddress("mu_etCore", &mu_etCore);
  }
  
  if(!b_mu_energyLossPar) {
    outputTree->SetBranchAddress("mu_energyLossPar", &mu_energyLossPar);
  }
  
  if(!b_mu_tracktheta) {
    outputTree->SetBranchAddress("mu_tracktheta", &mu_tracktheta);
  }
  
  if(!b_mu_muid_n) {
    outputTree->SetBranchAddress("mu_muid_n", &mu_muid_n);
  }
  
  if(!b_mu_muid_pt) {
    outputTree->SetBranchAddress("mu_muid_pt", &mu_muid_pt);
  }
  
  if(!b_mu_muid_eta) {
    outputTree->SetBranchAddress("mu_muid_eta", &mu_muid_eta);
  }
  
  if(!b_mu_muid_phi) {
    outputTree->SetBranchAddress("mu_muid_phi", &mu_muid_phi);
  }
  
  if(!b_mu_muid_isCombinedMuon) {
    outputTree->SetBranchAddress("mu_muid_isCombinedMuon", &mu_muid_isCombinedMuon);
  }
  
  if(!b_mu_muid_isStandAloneMuon) {
    outputTree->SetBranchAddress("mu_muid_isStandAloneMuon", &mu_muid_isStandAloneMuon);
  }
  
  if(!b_mu_muid_isSegmentTaggedMuon) {
    outputTree->SetBranchAddress("mu_muid_isSegmentTaggedMuon", &mu_muid_isSegmentTaggedMuon);
  }
  
  if(!b_mu_muid_isCaloMuonId) {
    outputTree->SetBranchAddress("mu_muid_isCaloMuonId", &mu_muid_isCaloMuonId);
  }
  
  if(!b_mu_muid_loose) {
    outputTree->SetBranchAddress("mu_muid_loose", &mu_muid_loose);
  }
  
  if(!b_mu_muid_medium) {
    outputTree->SetBranchAddress("mu_muid_medium", &mu_muid_medium);
  }
  
  if(!b_mu_muid_tight) {
    outputTree->SetBranchAddress("mu_muid_tight", &mu_muid_tight);
  }
  
  if(!b_mu_muid_charge) {
    outputTree->SetBranchAddress("mu_muid_charge", &mu_muid_charge);
  }
  
  if(!b_mu_muid_id_theta_exPV) {
    outputTree->SetBranchAddress("mu_muid_id_theta_exPV", &mu_muid_id_theta_exPV);
  }
  
  if(!b_mu_muid_me_theta_exPV) {
    outputTree->SetBranchAddress("mu_muid_me_theta_exPV", &mu_muid_me_theta_exPV);
  }
  
  if(!b_mu_muid_ms_theta) {
    outputTree->SetBranchAddress("mu_muid_ms_theta", &mu_muid_ms_theta);
  }
  
  if(!b_mu_muid_id_qoverp_exPV) {
    outputTree->SetBranchAddress("mu_muid_id_qoverp_exPV", &mu_muid_id_qoverp_exPV);
  }
  
  if(!b_mu_muid_me_qoverp_exPV) {
    outputTree->SetBranchAddress("mu_muid_me_qoverp_exPV", &mu_muid_me_qoverp_exPV);
  }
  
  if(!b_mu_muid_ms_qoverp) {
    outputTree->SetBranchAddress("mu_muid_ms_qoverp", &mu_muid_ms_qoverp);
  }
  
  if(!b_mu_muid_id_phi_exPV) {
    outputTree->SetBranchAddress("mu_muid_id_phi_exPV", &mu_muid_id_phi_exPV);
  }
  
  if(!b_mu_muid_me_phi_exPV) {
    outputTree->SetBranchAddress("mu_muid_me_phi_exPV", &mu_muid_me_phi_exPV);
  }
  
  if(!b_mu_muid_ms_phi) {
    outputTree->SetBranchAddress("mu_muid_ms_phi", &mu_muid_ms_phi);
  }
  
  if(!b_mu_muid_trackd0pvunbiased) {
    outputTree->SetBranchAddress("mu_muid_trackd0pvunbiased", &mu_muid_trackd0pvunbiased);
  }
  
  if(!b_mu_muid_trackz0pvunbiased) {
    outputTree->SetBranchAddress("mu_muid_trackz0pvunbiased", &mu_muid_trackz0pvunbiased);
  }
  
  if(!b_mu_muid_tracksigd0pvunbiased) {
    outputTree->SetBranchAddress("mu_muid_tracksigd0pvunbiased", &mu_muid_tracksigd0pvunbiased);
  }
  
  if(!b_mu_muid_tracksigz0pvunbiased) {
    outputTree->SetBranchAddress("mu_muid_tracksigz0pvunbiased", &mu_muid_tracksigz0pvunbiased);
  }
  
  if(!b_mu_muid_expectBLayerHit) {
    outputTree->SetBranchAddress("mu_muid_expectBLayerHit", &mu_muid_expectBLayerHit);
  }
  
  if(!b_mu_muid_nBLHits) {
    outputTree->SetBranchAddress("mu_muid_nBLHits", &mu_muid_nBLHits);
  }
  
  if(!b_mu_muid_nPixHits) {
    outputTree->SetBranchAddress("mu_muid_nPixHits", &mu_muid_nPixHits);
  }
  
  if(!b_mu_muid_nPixelDeadSensors) {
    outputTree->SetBranchAddress("mu_muid_nPixelDeadSensors", &mu_muid_nPixelDeadSensors);
  }
  
  if(!b_mu_muid_nSCTHits) {
    outputTree->SetBranchAddress("mu_muid_nSCTHits", &mu_muid_nSCTHits);
  }
  
  if(!b_mu_muid_nSCTDeadSensors) {
    outputTree->SetBranchAddress("mu_muid_nSCTDeadSensors", &mu_muid_nSCTDeadSensors);
  }
  
  if(!b_mu_muid_nPixHoles) {
    outputTree->SetBranchAddress("mu_muid_nPixHoles", &mu_muid_nPixHoles);
  }
  
  if(!b_mu_muid_nSCTHoles) {
    outputTree->SetBranchAddress("mu_muid_nSCTHoles", &mu_muid_nSCTHoles);
  }
  
  if(!b_mu_muid_nTRTHits) {
    outputTree->SetBranchAddress("mu_muid_nTRTHits", &mu_muid_nTRTHits);
  }
  
  if(!b_mu_muid_nTRTOutliers) {
    outputTree->SetBranchAddress("mu_muid_nTRTOutliers", &mu_muid_nTRTOutliers);
  }
  
  if(!b_mu_muid_etcone20) {
    outputTree->SetBranchAddress("mu_muid_etcone20", &mu_muid_etcone20);
  }
  
  if(!b_mu_muid_etcone30) {
    outputTree->SetBranchAddress("mu_muid_etcone30", &mu_muid_etcone30);
  }
  
  if(!b_mu_muid_etcone40) {
    outputTree->SetBranchAddress("mu_muid_etcone40", &mu_muid_etcone40);
  }
  
  if(!b_mu_muid_ptcone20) {
    outputTree->SetBranchAddress("mu_muid_ptcone20", &mu_muid_ptcone20);
  }
  
  if(!b_mu_muid_ptcone30) {
    outputTree->SetBranchAddress("mu_muid_ptcone30", &mu_muid_ptcone30);
  }
  
  if(!b_mu_muid_ptcone40) {
    outputTree->SetBranchAddress("mu_muid_ptcone40", &mu_muid_ptcone40);
  }
  
  if(!b_mu_muid_EFMG_index) {
    outputTree->SetBranchAddress("mu_muid_EFMG_index", &mu_muid_EFMG_index);
  }
  
  if(!b_mu_muid_isLowPtReconstructedMuon) {
    outputTree->SetBranchAddress("mu_muid_isLowPtReconstructedMuon", &mu_muid_isLowPtReconstructedMuon);
  }
  
  if(!b_mu_muid_etCore) {
    outputTree->SetBranchAddress("mu_muid_etCore", &mu_muid_etCore);
  }
  
  if(!b_mu_muid_energyLossPar) {
    outputTree->SetBranchAddress("mu_muid_energyLossPar", &mu_muid_energyLossPar);
  }
  
  if(!b_mu_muid_tracktheta) {
    outputTree->SetBranchAddress("mu_muid_tracktheta", &mu_muid_tracktheta);
  }
  
  if(!b_mu_staco_n) {
    outputTree->SetBranchAddress("mu_staco_n", &mu_staco_n);
  }
  
  if(!b_mu_staco_pt) {
    outputTree->SetBranchAddress("mu_staco_pt", &mu_staco_pt);
  }
  
  if(!b_mu_staco_eta) {
    outputTree->SetBranchAddress("mu_staco_eta", &mu_staco_eta);
  }
  
  if(!b_mu_staco_phi) {
    outputTree->SetBranchAddress("mu_staco_phi", &mu_staco_phi);
  }
  
  if(!b_mu_staco_isCombinedMuon) {
    outputTree->SetBranchAddress("mu_staco_isCombinedMuon", &mu_staco_isCombinedMuon);
  }
  
  if(!b_mu_staco_isStandAloneMuon) {
    outputTree->SetBranchAddress("mu_staco_isStandAloneMuon", &mu_staco_isStandAloneMuon);
  }
  
  if(!b_mu_staco_isSegmentTaggedMuon) {
    outputTree->SetBranchAddress("mu_staco_isSegmentTaggedMuon", &mu_staco_isSegmentTaggedMuon);
  }
  
  if(!b_mu_staco_isCaloMuonId) {
    outputTree->SetBranchAddress("mu_staco_isCaloMuonId", &mu_staco_isCaloMuonId);
  }
  
  if(!b_mu_staco_loose) {
    outputTree->SetBranchAddress("mu_staco_loose", &mu_staco_loose);
  }
  
  if(!b_mu_staco_medium) {
    outputTree->SetBranchAddress("mu_staco_medium", &mu_staco_medium);
  }
  
  if(!b_mu_staco_tight) {
    outputTree->SetBranchAddress("mu_staco_tight", &mu_staco_tight);
  }
  
  if(!b_mu_staco_charge) {
    outputTree->SetBranchAddress("mu_staco_charge", &mu_staco_charge);
  }
  
  if(!b_mu_staco_id_theta_exPV) {
    outputTree->SetBranchAddress("mu_staco_id_theta_exPV", &mu_staco_id_theta_exPV);
  }
  
  if(!b_mu_staco_me_theta_exPV) {
    outputTree->SetBranchAddress("mu_staco_me_theta_exPV", &mu_staco_me_theta_exPV);
  }
  
  if(!b_mu_staco_ms_theta) {
    outputTree->SetBranchAddress("mu_staco_ms_theta", &mu_staco_ms_theta);
  }
  
  if(!b_mu_staco_id_qoverp_exPV) {
    outputTree->SetBranchAddress("mu_staco_id_qoverp_exPV", &mu_staco_id_qoverp_exPV);
  }
  
  if(!b_mu_staco_me_qoverp_exPV) {
    outputTree->SetBranchAddress("mu_staco_me_qoverp_exPV", &mu_staco_me_qoverp_exPV);
  }
  
  if(!b_mu_staco_ms_qoverp) {
    outputTree->SetBranchAddress("mu_staco_ms_qoverp", &mu_staco_ms_qoverp);
  }
  
  if(!b_mu_staco_id_phi_exPV) {
    outputTree->SetBranchAddress("mu_staco_id_phi_exPV", &mu_staco_id_phi_exPV);
  }
  
  if(!b_mu_staco_me_phi_exPV) {
    outputTree->SetBranchAddress("mu_staco_me_phi_exPV", &mu_staco_me_phi_exPV);
  }
  
  if(!b_mu_staco_ms_phi) {
    outputTree->SetBranchAddress("mu_staco_ms_phi", &mu_staco_ms_phi);
  }
  
  if(!b_mu_staco_trackd0pvunbiased) {
    outputTree->SetBranchAddress("mu_staco_trackd0pvunbiased", &mu_staco_trackd0pvunbiased);
  }
  
  if(!b_mu_staco_trackz0pvunbiased) {
    outputTree->SetBranchAddress("mu_staco_trackz0pvunbiased", &mu_staco_trackz0pvunbiased);
  }
  
  if(!b_mu_staco_tracksigd0pvunbiased) {
    outputTree->SetBranchAddress("mu_staco_tracksigd0pvunbiased", &mu_staco_tracksigd0pvunbiased);
  }
  
  if(!b_mu_staco_tracksigz0pvunbiased) {
    outputTree->SetBranchAddress("mu_staco_tracksigz0pvunbiased", &mu_staco_tracksigz0pvunbiased);
  }
  
  if(!b_mu_staco_expectBLayerHit) {
    outputTree->SetBranchAddress("mu_staco_expectBLayerHit", &mu_staco_expectBLayerHit);
  }
  
  if(!b_mu_staco_nBLHits) {
    outputTree->SetBranchAddress("mu_staco_nBLHits", &mu_staco_nBLHits);
  }
  
  if(!b_mu_staco_nPixHits) {
    outputTree->SetBranchAddress("mu_staco_nPixHits", &mu_staco_nPixHits);
  }
  
  if(!b_mu_staco_nPixelDeadSensors) {
    outputTree->SetBranchAddress("mu_staco_nPixelDeadSensors", &mu_staco_nPixelDeadSensors);
  }
  
  if(!b_mu_staco_nSCTHits) {
    outputTree->SetBranchAddress("mu_staco_nSCTHits", &mu_staco_nSCTHits);
  }
  
  if(!b_mu_staco_nSCTDeadSensors) {
    outputTree->SetBranchAddress("mu_staco_nSCTDeadSensors", &mu_staco_nSCTDeadSensors);
  }
  
  if(!b_mu_staco_nPixHoles) {
    outputTree->SetBranchAddress("mu_staco_nPixHoles", &mu_staco_nPixHoles);
  }
  
  if(!b_mu_staco_nSCTHoles) {
    outputTree->SetBranchAddress("mu_staco_nSCTHoles", &mu_staco_nSCTHoles);
  }
  
  if(!b_mu_staco_nTRTHits) {
    outputTree->SetBranchAddress("mu_staco_nTRTHits", &mu_staco_nTRTHits);
  }
  
  if(!b_mu_staco_nTRTOutliers) {
    outputTree->SetBranchAddress("mu_staco_nTRTOutliers", &mu_staco_nTRTOutliers);
  }
  
  if(!b_mu_staco_etcone20) {
    outputTree->SetBranchAddress("mu_staco_etcone20", &mu_staco_etcone20);
  }
  
  if(!b_mu_staco_etcone30) {
    outputTree->SetBranchAddress("mu_staco_etcone30", &mu_staco_etcone30);
  }
  
  if(!b_mu_staco_etcone40) {
    outputTree->SetBranchAddress("mu_staco_etcone40", &mu_staco_etcone40);
  }
  
  if(!b_mu_staco_ptcone20) {
    outputTree->SetBranchAddress("mu_staco_ptcone20", &mu_staco_ptcone20);
  }
  
  if(!b_mu_staco_ptcone30) {
    outputTree->SetBranchAddress("mu_staco_ptcone30", &mu_staco_ptcone30);
  }
  
  if(!b_mu_staco_ptcone40) {
    outputTree->SetBranchAddress("mu_staco_ptcone40", &mu_staco_ptcone40);
  }
  
  if(!b_mu_staco_EFMG_index) {
    outputTree->SetBranchAddress("mu_staco_EFMG_index", &mu_staco_EFMG_index);
  }
  
  if(!b_mu_staco_isLowPtReconstructedMuon) {
    outputTree->SetBranchAddress("mu_staco_isLowPtReconstructedMuon", &mu_staco_isLowPtReconstructedMuon);
  }
  
  if(!b_mu_staco_etCore) {
    outputTree->SetBranchAddress("mu_staco_etCore", &mu_staco_etCore);
  }
  
  if(!b_mu_staco_energyLossPar) {
    outputTree->SetBranchAddress("mu_staco_energyLossPar", &mu_staco_energyLossPar);
  }
  
  if(!b_mu_staco_tracktheta) {
    outputTree->SetBranchAddress("mu_staco_tracktheta", &mu_staco_tracktheta);
  }
  
  if(!b_mu_staco_me_qoverp) {
    outputTree->SetBranchAddress("mu_staco_me_qoverp", &mu_staco_me_qoverp);
  }
  
  if(!b_mu_staco_me_theta) {
    outputTree->SetBranchAddress("mu_staco_me_theta", &mu_staco_me_theta);
  }
  
  if(!b_mu_staco_me_phi) {
    outputTree->SetBranchAddress("mu_staco_me_phi", &mu_staco_me_phi);
  }
  
  if(!b_mu_staco_id_theta) {
    outputTree->SetBranchAddress("mu_staco_id_theta", &mu_staco_id_theta);
  }
  
  if(!b_mu_staco_id_phi) {
    outputTree->SetBranchAddress("mu_staco_id_phi", &mu_staco_id_phi);
  }
  
  if(!b_mu_staco_id_qoverp) {
    outputTree->SetBranchAddress("mu_staco_id_qoverp", &mu_staco_id_qoverp);
  }
  
  if(!b_el_E) {
    outputTree->SetBranchAddress("el_E", &el_E);
  }
  
  if(!b_el_Et) {
    outputTree->SetBranchAddress("el_Et", &el_Et);
  }
  
  if(!b_el_pt) {
    outputTree->SetBranchAddress("el_pt", &el_pt);
  }
  
  if(!b_el_m) {
    outputTree->SetBranchAddress("el_m", &el_m);
  }
  
  if(!b_el_eta) {
    outputTree->SetBranchAddress("el_eta", &el_eta);
  }
  
  if(!b_el_phi) {
    outputTree->SetBranchAddress("el_phi", &el_phi);
  }
  
  if(!b_el_trackd0pv) {
    outputTree->SetBranchAddress("el_trackd0pv", &el_trackd0pv);
  }
  
  if(!b_el_trackz0pv) {
    outputTree->SetBranchAddress("el_trackz0pv", &el_trackz0pv);
  }
  
  if(!b_el_tracksigd0pv) {
    outputTree->SetBranchAddress("el_tracksigd0pv", &el_tracksigd0pv);
  }
  
  if(!b_el_tracksigz0pv) {
    outputTree->SetBranchAddress("el_tracksigz0pv", &el_tracksigz0pv);
  }
  
  if(!b_el_trackd0pvunbiased) {
    outputTree->SetBranchAddress("el_trackd0pvunbiased", &el_trackd0pvunbiased);
  }
  
  if(!b_el_trackz0pvunbiased) {
    outputTree->SetBranchAddress("el_trackz0pvunbiased", &el_trackz0pvunbiased);
  }
  
  if(!b_el_tracksigd0pvunbiased) {
    outputTree->SetBranchAddress("el_tracksigd0pvunbiased", &el_tracksigd0pvunbiased);
  }
  
  if(!b_el_tracksigz0pvunbiased) {
    outputTree->SetBranchAddress("el_tracksigz0pvunbiased", &el_tracksigz0pvunbiased);
  }
  
  if(!b_el_charge) {
    outputTree->SetBranchAddress("el_charge", &el_charge);
  }
  
  if(!b_el_loose) {
    outputTree->SetBranchAddress("el_loose", &el_loose);
  }
  
  if(!b_el_loosePP) {
    outputTree->SetBranchAddress("el_loosePP", &el_loosePP);
  }
  
  if(!b_el_mediumWithoutTrack) {
    outputTree->SetBranchAddress("el_mediumWithoutTrack", &el_mediumWithoutTrack);
  }
  
  if(!b_el_mediumPP) {
    outputTree->SetBranchAddress("el_mediumPP", &el_mediumPP);
  }
  
  if(!b_el_tightPP) {
    outputTree->SetBranchAddress("el_tightPP", &el_tightPP);
  }
  
  if(!b_el_author) {
    outputTree->SetBranchAddress("el_author", &el_author);
  }
  
  if(!b_el_OQ) {
    outputTree->SetBranchAddress("el_OQ", &el_OQ);
  }
  
  if(!b_el_n) {
    outputTree->SetBranchAddress("el_n", &el_n);
  }
  
  if(!b_el_cl_E) {
    outputTree->SetBranchAddress("el_cl_E", &el_cl_E);
  }
  
  if(!b_el_cl_pt) {
    outputTree->SetBranchAddress("el_cl_pt", &el_cl_pt);
  }
  
  if(!b_el_cl_eta) {
    outputTree->SetBranchAddress("el_cl_eta", &el_cl_eta);
  }
  
  if(!b_el_cl_phi) {
    outputTree->SetBranchAddress("el_cl_phi", &el_cl_phi);
  }
  
  if(!b_el_tracketa) {
    outputTree->SetBranchAddress("el_tracketa", &el_tracketa);
  }
  
  if(!b_el_trackphi) {
    outputTree->SetBranchAddress("el_trackphi", &el_trackphi);
  }
  
  if(!b_el_trackpt) {
    outputTree->SetBranchAddress("el_trackpt", &el_trackpt);
  }
  
  if(!b_el_tracktheta) {
    outputTree->SetBranchAddress("el_tracktheta", &el_tracktheta);
  }
  
  if(!b_el_etas2) {
    outputTree->SetBranchAddress("el_etas2", &el_etas2);
  }
  
  if(!b_el_etap) {
    outputTree->SetBranchAddress("el_etap", &el_etap);
  }
  
  if(!b_el_nSCTHits) {
    outputTree->SetBranchAddress("el_nSCTHits", &el_nSCTHits);
  }
  
  if(!b_el_Etcone20) {
    outputTree->SetBranchAddress("el_Etcone20", &el_Etcone20);
  }
  
  if(!b_el_Etcone30) {
    outputTree->SetBranchAddress("el_Etcone30", &el_Etcone30);
  }
  
  if(!b_el_Etcone40) {
    outputTree->SetBranchAddress("el_Etcone40", &el_Etcone40);
  }
  
  if(!b_el_topoEtcone20) {
    outputTree->SetBranchAddress("el_topoEtcone20", &el_topoEtcone20);
  }
  
  if(!b_el_topoEtcone30) {
    outputTree->SetBranchAddress("el_topoEtcone30", &el_topoEtcone30);
  }
  
  if(!b_el_topoEtcone40) {
    outputTree->SetBranchAddress("el_topoEtcone40", &el_topoEtcone40);
  }
  
  if(!b_el_ptcone20) {
    outputTree->SetBranchAddress("el_ptcone20", &el_ptcone20);
  }
  
  if(!b_el_ptcone30) {
    outputTree->SetBranchAddress("el_ptcone30", &el_ptcone30);
  }
  
  if(!b_el_ptcone40) {
    outputTree->SetBranchAddress("el_ptcone40", &el_ptcone40);
  }
  
  if(!b_el_L1_dr) {
    outputTree->SetBranchAddress("el_L1_dr", &el_L1_dr);
  }
  
  if(!b_el_L2_dr) {
    outputTree->SetBranchAddress("el_L2_dr", &el_L2_dr);
  }
  
  if(!b_el_EF_dr) {
    outputTree->SetBranchAddress("el_EF_dr", &el_EF_dr);
  }
  
  if(!b_el_EF_index) {
    outputTree->SetBranchAddress("el_EF_index", &el_EF_index);
  }
  
  if(!b_el_Ethad) {
    outputTree->SetBranchAddress("el_Ethad", &el_Ethad);
  }
  
  if(!b_el_Ethad1) {
    outputTree->SetBranchAddress("el_Ethad1", &el_Ethad1);
  }
  
  if(!b_el_reta) {
    outputTree->SetBranchAddress("el_reta", &el_reta);
  }
  
  if(!b_el_weta2) {
    outputTree->SetBranchAddress("el_weta2", &el_weta2);
  }
  
  if(!b_el_f1) {
    outputTree->SetBranchAddress("el_f1", &el_f1);
  }
  
  if(!b_el_f3) {
    outputTree->SetBranchAddress("el_f3", &el_f3);
  }
  
  if(!b_el_wstot) {
    outputTree->SetBranchAddress("el_wstot", &el_wstot);
  }
  
  if(!b_el_emaxs1) {
    outputTree->SetBranchAddress("el_emaxs1", &el_emaxs1);
  }
  
  if(!b_el_Emax2) {
    outputTree->SetBranchAddress("el_Emax2", &el_Emax2);
  }
  
  if(!b_el_deltaeta1) {
    outputTree->SetBranchAddress("el_deltaeta1", &el_deltaeta1);
  }
  
  if(!b_el_deltaphi2) {
    outputTree->SetBranchAddress("el_deltaphi2", &el_deltaphi2);
  }
  
  if(!b_el_trackd0_physics) {
    outputTree->SetBranchAddress("el_trackd0_physics", &el_trackd0_physics);
  }
  
  if(!b_el_trackqoverp) {
    outputTree->SetBranchAddress("el_trackqoverp", &el_trackqoverp);
  }
  
  if(!b_el_TRTHighTOutliersRatio) {
    outputTree->SetBranchAddress("el_TRTHighTOutliersRatio", &el_TRTHighTOutliersRatio);
  }
  
  if(!b_el_nTRTHits) {
    outputTree->SetBranchAddress("el_nTRTHits", &el_nTRTHits);
  }
  
  if(!b_el_nTRTOutliers) {
    outputTree->SetBranchAddress("el_nTRTOutliers", &el_nTRTOutliers);
  }
  
  if(!b_el_nSiHits) {
    outputTree->SetBranchAddress("el_nSiHits", &el_nSiHits);
  }
  
  if(!b_el_nSCTOutliers) {
    outputTree->SetBranchAddress("el_nSCTOutliers", &el_nSCTOutliers);
  }
  
  if(!b_el_nPixHits) {
    outputTree->SetBranchAddress("el_nPixHits", &el_nPixHits);
  }
  
  if(!b_el_nPixelOutliers) {
    outputTree->SetBranchAddress("el_nPixelOutliers", &el_nPixelOutliers);
  }
  
  if(!b_el_nBLHits) {
    outputTree->SetBranchAddress("el_nBLHits", &el_nBLHits);
  }
  
  if(!b_el_nBLayerOutliers) {
    outputTree->SetBranchAddress("el_nBLayerOutliers", &el_nBLayerOutliers);
  }
  
  if(!b_el_expectHitInBLayer) {
    outputTree->SetBranchAddress("el_expectHitInBLayer", &el_expectHitInBLayer);
  }
  
  if(!b_el_isEM) {
    outputTree->SetBranchAddress("el_isEM", &el_isEM);
  }
  
  if(!b_el_ED_median) {
    outputTree->SetBranchAddress("el_ED_median", &el_ED_median);
  }
  
  if(!b_jet_AntiKt4TopoEM_n) {
    outputTree->SetBranchAddress("jet_AntiKt4TopoEM_n", &jet_AntiKt4TopoEM_n);
  }
  
  if(!b_jet_AntiKt4TopoEM_pt) {
    outputTree->SetBranchAddress("jet_AntiKt4TopoEM_pt", &jet_AntiKt4TopoEM_pt);
  }
  
  if(!b_jet_AntiKt4TopoEM_eta) {
    outputTree->SetBranchAddress("jet_AntiKt4TopoEM_eta", &jet_AntiKt4TopoEM_eta);
  }
  
  if(!b_jet_AntiKt4TopoEM_phi) {
    outputTree->SetBranchAddress("jet_AntiKt4TopoEM_phi", &jet_AntiKt4TopoEM_phi);
  }
  
  if(!b_jet_AntiKt4TopoEM_m) {
    outputTree->SetBranchAddress("jet_AntiKt4TopoEM_m", &jet_AntiKt4TopoEM_m);
  }
  
  if(!b_jet_AntiKt4TopoEM_EtaOrigin) {
    outputTree->SetBranchAddress("jet_AntiKt4TopoEM_EtaOrigin", &jet_AntiKt4TopoEM_EtaOrigin);
  }
  
  if(!b_jet_AntiKt4TopoEM_PhiOrigin) {
    outputTree->SetBranchAddress("jet_AntiKt4TopoEM_PhiOrigin", &jet_AntiKt4TopoEM_PhiOrigin);
  }
  
  if(!b_jet_AntiKt4TopoEM_MOrigin) {
    outputTree->SetBranchAddress("jet_AntiKt4TopoEM_MOrigin", &jet_AntiKt4TopoEM_MOrigin);
  }
  
  if(!b_jet_AntiKt4TopoEM_emscale_E) {
    outputTree->SetBranchAddress("jet_AntiKt4TopoEM_emscale_E", &jet_AntiKt4TopoEM_emscale_E);
  }
  
  if(!b_jet_AntiKt4TopoEM_emscale_pt) {
    outputTree->SetBranchAddress("jet_AntiKt4TopoEM_emscale_pt", &jet_AntiKt4TopoEM_emscale_pt);
  }
  
  if(!b_jet_AntiKt4TopoEM_emscale_eta) {
    outputTree->SetBranchAddress("jet_AntiKt4TopoEM_emscale_eta", &jet_AntiKt4TopoEM_emscale_eta);
  }
  
  if(!b_jet_AntiKt4TopoEM_emscale_phi) {
    outputTree->SetBranchAddress("jet_AntiKt4TopoEM_emscale_phi", &jet_AntiKt4TopoEM_emscale_phi);
  }
  
  if(!b_jet_AntiKt4TopoEM_emscale_m) {
    outputTree->SetBranchAddress("jet_AntiKt4TopoEM_emscale_m", &jet_AntiKt4TopoEM_emscale_m);
  }
  
  if(!b_jet_AntiKt4TopoEM_emfrac) {
    outputTree->SetBranchAddress("jet_AntiKt4TopoEM_emfrac", &jet_AntiKt4TopoEM_emfrac);
  }
  
  if(!b_jet_AntiKt4TopoEM_hecf) {
    outputTree->SetBranchAddress("jet_AntiKt4TopoEM_hecf", &jet_AntiKt4TopoEM_hecf);
  }
  
  if(!b_jet_AntiKt4TopoEM_LArQuality) {
    outputTree->SetBranchAddress("jet_AntiKt4TopoEM_LArQuality", &jet_AntiKt4TopoEM_LArQuality);
  }
  
  if(!b_jet_AntiKt4TopoEM_HECQuality) {
    outputTree->SetBranchAddress("jet_AntiKt4TopoEM_HECQuality", &jet_AntiKt4TopoEM_HECQuality);
  }
  
  if(!b_jet_AntiKt4TopoEM_Timing) {
    outputTree->SetBranchAddress("jet_AntiKt4TopoEM_Timing", &jet_AntiKt4TopoEM_Timing);
  }
  
  if(!b_jet_AntiKt4TopoEM_sumPtTrk) {
    outputTree->SetBranchAddress("jet_AntiKt4TopoEM_sumPtTrk", &jet_AntiKt4TopoEM_sumPtTrk);
  }
  
  if(!b_jet_AntiKt4TopoEM_SamplingMax) {
    outputTree->SetBranchAddress("jet_AntiKt4TopoEM_SamplingMax", &jet_AntiKt4TopoEM_SamplingMax);
  }
  
  if(!b_jet_AntiKt4TopoEM_fracSamplingMax) {
    outputTree->SetBranchAddress("jet_AntiKt4TopoEM_fracSamplingMax", &jet_AntiKt4TopoEM_fracSamplingMax);
  }
  
  if(!b_jet_AntiKt4TopoEM_NegativeE) {
    outputTree->SetBranchAddress("jet_AntiKt4TopoEM_NegativeE", &jet_AntiKt4TopoEM_NegativeE);
  }
  
  if(!b_jet_AntiKt4TopoEM_AverageLArQF) {
    outputTree->SetBranchAddress("jet_AntiKt4TopoEM_AverageLArQF", &jet_AntiKt4TopoEM_AverageLArQF);
  }
  
  if(!b_jet_AntiKt4TopoEM_isUgly) {
    outputTree->SetBranchAddress("jet_AntiKt4TopoEM_isUgly", &jet_AntiKt4TopoEM_isUgly);
  }
  
  if(!b_jet_AntiKt4TopoEM_isBadLooseMinus) {
    outputTree->SetBranchAddress("jet_AntiKt4TopoEM_isBadLooseMinus", &jet_AntiKt4TopoEM_isBadLooseMinus);
  }
  
  if(!b_jet_AntiKt4TopoEM_isBadLoose) {
    outputTree->SetBranchAddress("jet_AntiKt4TopoEM_isBadLoose", &jet_AntiKt4TopoEM_isBadLoose);
  }
  
  if(!b_jet_AntiKt4TopoEM_EMJES) {
    outputTree->SetBranchAddress("jet_AntiKt4TopoEM_EMJES", &jet_AntiKt4TopoEM_EMJES);
  }
  
  if(!b_jet_AntiKt4TopoEM_BCH_CORR_JET) {
    outputTree->SetBranchAddress("jet_AntiKt4TopoEM_BCH_CORR_JET", &jet_AntiKt4TopoEM_BCH_CORR_JET);
  }
  
  if(!b_jet_AntiKt4TopoEM_BCH_CORR_CELL) {
    outputTree->SetBranchAddress("jet_AntiKt4TopoEM_BCH_CORR_CELL", &jet_AntiKt4TopoEM_BCH_CORR_CELL);
  }
  
  if(!b_jet_AntiKt4TopoEM_jvtxf) {
    outputTree->SetBranchAddress("jet_AntiKt4TopoEM_jvtxf", &jet_AntiKt4TopoEM_jvtxf);
  }
  
  if(!b_jet_AntiKt4TopoEM_flavor_weight_Comb) {
    outputTree->SetBranchAddress("jet_AntiKt4TopoEM_flavor_weight_Comb", &jet_AntiKt4TopoEM_flavor_weight_Comb);
  }
  
  if(!b_jet_AntiKt4TopoEM_flavor_weight_IP2D) {
    outputTree->SetBranchAddress("jet_AntiKt4TopoEM_flavor_weight_IP2D", &jet_AntiKt4TopoEM_flavor_weight_IP2D);
  }
  
  if(!b_jet_AntiKt4TopoEM_flavor_weight_IP3D) {
    outputTree->SetBranchAddress("jet_AntiKt4TopoEM_flavor_weight_IP3D", &jet_AntiKt4TopoEM_flavor_weight_IP3D);
  }
  
  if(!b_jet_AntiKt4TopoEM_flavor_weight_SV0) {
    outputTree->SetBranchAddress("jet_AntiKt4TopoEM_flavor_weight_SV0", &jet_AntiKt4TopoEM_flavor_weight_SV0);
  }
  
  if(!b_jet_AntiKt4TopoEM_flavor_weight_SV1) {
    outputTree->SetBranchAddress("jet_AntiKt4TopoEM_flavor_weight_SV1", &jet_AntiKt4TopoEM_flavor_weight_SV1);
  }
  
  if(!b_jet_AntiKt4TopoEM_flavor_weight_SV2) {
    outputTree->SetBranchAddress("jet_AntiKt4TopoEM_flavor_weight_SV2", &jet_AntiKt4TopoEM_flavor_weight_SV2);
  }
  
  if(!b_jet_AntiKt4TopoEM_flavor_weight_SoftMuonTagChi2) {
    outputTree->SetBranchAddress("jet_AntiKt4TopoEM_flavor_weight_SoftMuonTagChi2", &jet_AntiKt4TopoEM_flavor_weight_SoftMuonTagChi2);
  }
  
  if(!b_jet_AntiKt4TopoEM_flavor_weight_SecondSoftMuonTagChi2) {
    outputTree->SetBranchAddress("jet_AntiKt4TopoEM_flavor_weight_SecondSoftMuonTagChi2", &jet_AntiKt4TopoEM_flavor_weight_SecondSoftMuonTagChi2);
  }
  
  if(!b_jet_AntiKt4TopoEM_flavor_weight_JetFitterTagNN) {
    outputTree->SetBranchAddress("jet_AntiKt4TopoEM_flavor_weight_JetFitterTagNN", &jet_AntiKt4TopoEM_flavor_weight_JetFitterTagNN);
  }
  
  if(!b_jet_AntiKt4TopoEM_flavor_weight_JetFitterCOMBNN) {
    outputTree->SetBranchAddress("jet_AntiKt4TopoEM_flavor_weight_JetFitterCOMBNN", &jet_AntiKt4TopoEM_flavor_weight_JetFitterCOMBNN);
  }
  
  if(!b_jet_AntiKt4TopoEM_flavor_weight_GbbNN) {
    outputTree->SetBranchAddress("jet_AntiKt4TopoEM_flavor_weight_GbbNN", &jet_AntiKt4TopoEM_flavor_weight_GbbNN);
  }
  
  if(!b_jet_AntiKt4TopoEM_flavor_weight_MV1) {
    outputTree->SetBranchAddress("jet_AntiKt4TopoEM_flavor_weight_MV1", &jet_AntiKt4TopoEM_flavor_weight_MV1);
  }
  
  if(!b_jet_AntiKt4TopoEM_flavor_weight_MV2) {
    outputTree->SetBranchAddress("jet_AntiKt4TopoEM_flavor_weight_MV2", &jet_AntiKt4TopoEM_flavor_weight_MV2);
  }
  
  if(!b_jet_AntiKt4TopoEM_ActiveArea) {
    outputTree->SetBranchAddress("jet_AntiKt4TopoEM_ActiveArea", &jet_AntiKt4TopoEM_ActiveArea);
  }
  
  if(!b_jet_AntiKt4TopoEM_ActiveAreaPx) {
    outputTree->SetBranchAddress("jet_AntiKt4TopoEM_ActiveAreaPx", &jet_AntiKt4TopoEM_ActiveAreaPx);
  }
  
  if(!b_jet_AntiKt4TopoEM_ActiveAreaPy) {
    outputTree->SetBranchAddress("jet_AntiKt4TopoEM_ActiveAreaPy", &jet_AntiKt4TopoEM_ActiveAreaPy);
  }
  
  if(!b_jet_AntiKt4TopoEM_ActiveAreaPz) {
    outputTree->SetBranchAddress("jet_AntiKt4TopoEM_ActiveAreaPz", &jet_AntiKt4TopoEM_ActiveAreaPz);
  }
  
  if(!b_jet_AntiKt4TopoEM_ActiveAreaE) {
    outputTree->SetBranchAddress("jet_AntiKt4TopoEM_ActiveAreaE", &jet_AntiKt4TopoEM_ActiveAreaE);
  }
  
  if(!b_Eventshape_rhoKt3EM) {
    outputTree->SetBranchAddress("Eventshape_rhoKt3EM", &Eventshape_rhoKt3EM);
  }
  
  if(!b_Eventshape_rhoKt4EM) {
    outputTree->SetBranchAddress("Eventshape_rhoKt4EM", &Eventshape_rhoKt4EM);
  }
  
  if(!b_Eventshape_rhoKt3LC) {
    outputTree->SetBranchAddress("Eventshape_rhoKt3LC", &Eventshape_rhoKt3LC);
  }
  
  if(!b_Eventshape_rhoKt4LC) {
    outputTree->SetBranchAddress("Eventshape_rhoKt4LC", &Eventshape_rhoKt4LC);
  }
  
  if(!b_MET_MuonBoy_et) {
    outputTree->SetBranchAddress("MET_MuonBoy_et", &MET_MuonBoy_et);
  }
  
  if(!b_MET_MuonBoy_phi) {
    outputTree->SetBranchAddress("MET_MuonBoy_phi", &MET_MuonBoy_phi);
  }
  
  if(!b_MET_RefMuon_Track_et) {
    outputTree->SetBranchAddress("MET_RefMuon_Track_et", &MET_RefMuon_Track_et);
  }
  
  if(!b_MET_RefMuon_Track_phi) {
    outputTree->SetBranchAddress("MET_RefMuon_Track_phi", &MET_RefMuon_Track_phi);
  }
  
  if(!b_MET_LocHadTopo_etx_ForwardReg) {
    outputTree->SetBranchAddress("MET_LocHadTopo_etx_ForwardReg", &MET_LocHadTopo_etx_ForwardReg);
  }
  
  if(!b_MET_LocHadTopo_etx_EndcapRegion) {
    outputTree->SetBranchAddress("MET_LocHadTopo_etx_EndcapRegion", &MET_LocHadTopo_etx_EndcapRegion);
  }
  
  if(!b_MET_LocHadTopo_etx_CentralReg) {
    outputTree->SetBranchAddress("MET_LocHadTopo_etx_CentralReg", &MET_LocHadTopo_etx_CentralReg);
  }
  
  if(!b_MET_LocHadTopo_ety_ForwardReg) {
    outputTree->SetBranchAddress("MET_LocHadTopo_ety_ForwardReg", &MET_LocHadTopo_ety_ForwardReg);
  }
  
  if(!b_MET_LocHadTopo_ety_EndcapRegion) {
    outputTree->SetBranchAddress("MET_LocHadTopo_ety_EndcapRegion", &MET_LocHadTopo_ety_EndcapRegion);
  }
  
  if(!b_MET_LocHadTopo_ety_CentralReg) {
    outputTree->SetBranchAddress("MET_LocHadTopo_ety_CentralReg", &MET_LocHadTopo_ety_CentralReg);
  }
  
  if(!b_MET_RefFinal_etx_CentralReg) {
    outputTree->SetBranchAddress("MET_RefFinal_etx_CentralReg", &MET_RefFinal_etx_CentralReg);
  }
  
  if(!b_MET_RefFinal_etx_EndcapRegion) {
    outputTree->SetBranchAddress("MET_RefFinal_etx_EndcapRegion", &MET_RefFinal_etx_EndcapRegion);
  }
  
  if(!b_MET_RefFinal_etx_ForwardReg) {
    outputTree->SetBranchAddress("MET_RefFinal_etx_ForwardReg", &MET_RefFinal_etx_ForwardReg);
  }
  
  if(!b_MET_RefFinal_ety_CentralReg) {
    outputTree->SetBranchAddress("MET_RefFinal_ety_CentralReg", &MET_RefFinal_ety_CentralReg);
  }
  
  if(!b_MET_RefFinal_ety_EndcapRegion) {
    outputTree->SetBranchAddress("MET_RefFinal_ety_EndcapRegion", &MET_RefFinal_ety_EndcapRegion);
  }
  
  if(!b_MET_RefFinal_ety_ForwardReg) {
    outputTree->SetBranchAddress("MET_RefFinal_ety_ForwardReg", &MET_RefFinal_ety_ForwardReg);
  }
  
  if(!b_MET_RefFinal_et) {
    outputTree->SetBranchAddress("MET_RefFinal_et", &MET_RefFinal_et);
  }
  
  if(!b_MET_RefFinal_phi) {
    outputTree->SetBranchAddress("MET_RefFinal_phi", &MET_RefFinal_phi);
  }
  
  if(!b_MET_RefFinal_sumet) {
    outputTree->SetBranchAddress("MET_RefFinal_sumet", &MET_RefFinal_sumet);
  }
  
  if(!b_MET_RefFinal_sumet_CentralReg) {
    outputTree->SetBranchAddress("MET_RefFinal_sumet_CentralReg", &MET_RefFinal_sumet_CentralReg);
  }
  
  if(!b_MET_RefFinal_sumet_EndcapRegion) {
    outputTree->SetBranchAddress("MET_RefFinal_sumet_EndcapRegion", &MET_RefFinal_sumet_EndcapRegion);
  }
  
  if(!b_MET_RefFinal_sumet_ForwardReg) {
    outputTree->SetBranchAddress("MET_RefFinal_sumet_ForwardReg", &MET_RefFinal_sumet_ForwardReg);
  }
  
  if(!b_vxp_n) {
    outputTree->SetBranchAddress("vxp_n", &vxp_n);
  }
  
  if(!b_vxp_x) {
    outputTree->SetBranchAddress("vxp_x", &vxp_x);
  }
  
  if(!b_vxp_y) {
    outputTree->SetBranchAddress("vxp_y", &vxp_y);
  }
  
  if(!b_vxp_z) {
    outputTree->SetBranchAddress("vxp_z", &vxp_z);
  }
  
  if(!b_vxp_err_x) {
    outputTree->SetBranchAddress("vxp_err_x", &vxp_err_x);
  }
  
  if(!b_vxp_err_y) {
    outputTree->SetBranchAddress("vxp_err_y", &vxp_err_y);
  }
  
  if(!b_vxp_err_z) {
    outputTree->SetBranchAddress("vxp_err_z", &vxp_err_z);
  }
  
  if(!b_vxp_type) {
    outputTree->SetBranchAddress("vxp_type", &vxp_type);
  }
  
  if(!b_vxp_chi2) {
    outputTree->SetBranchAddress("vxp_chi2", &vxp_chi2);
  }
  
  if(!b_vxp_ndof) {
    outputTree->SetBranchAddress("vxp_ndof", &vxp_ndof);
  }
  
  if(!b_vxp_px) {
    outputTree->SetBranchAddress("vxp_px", &vxp_px);
  }
  
  if(!b_vxp_py) {
    outputTree->SetBranchAddress("vxp_py", &vxp_py);
  }
  
  if(!b_vxp_pz) {
    outputTree->SetBranchAddress("vxp_pz", &vxp_pz);
  }
  
  if(!b_vxp_E) {
    outputTree->SetBranchAddress("vxp_E", &vxp_E);
  }
  
  if(!b_vxp_m) {
    outputTree->SetBranchAddress("vxp_m", &vxp_m);
  }
  
  if(!b_vxp_nTracks) {
    outputTree->SetBranchAddress("vxp_nTracks", &vxp_nTracks);
  }
  
  if(!b_vxp_sumPt) {
    outputTree->SetBranchAddress("vxp_sumPt", &vxp_sumPt);
  }
  
  if(!b_vxp_trk_weight) {
    outputTree->SetBranchAddress("vxp_trk_weight", &vxp_trk_weight);
  }
  
  if(!b_vxp_trk_n) {
    outputTree->SetBranchAddress("vxp_trk_n", &vxp_trk_n);
  }
  
  if(!b_vxp_trk_index) {
    outputTree->SetBranchAddress("vxp_trk_index", &vxp_trk_index);
  }
  
  if(!b_mcevt_weight) {
    outputTree->SetBranchAddress("mcevt_weight", &mcevt_weight);
  }
  
  if(!b_jet_AntiKt4TopoEM_flavor_truth_label) {
    outputTree->SetBranchAddress("jet_AntiKt4TopoEM_flavor_truth_label", &jet_AntiKt4TopoEM_flavor_truth_label);
  }
  
  if(!b_EF_e22vh_loose) {
    outputTree->SetBranchAddress("EF_e22vh_loose", &EF_e22vh_loose);
  }
  
  if(!b_EF_e22vh_loose0) {
    outputTree->SetBranchAddress("EF_e22vh_loose0", &EF_e22vh_loose0);
  }
  
  if(!b_EF_e22vh_loose1) {
    outputTree->SetBranchAddress("EF_e22vh_loose1", &EF_e22vh_loose1);
  }
  
  if(!b_EF_e22vh_medium1_IDTrkNoCut) {
    outputTree->SetBranchAddress("EF_e22vh_medium1_IDTrkNoCut", &EF_e22vh_medium1_IDTrkNoCut);
  }
  
  if(!b_EF_e22vhi_medium1) {
    outputTree->SetBranchAddress("EF_e22vhi_medium1", &EF_e22vhi_medium1);
  }
  
  if(!b_EF_e24vh_loose) {
    outputTree->SetBranchAddress("EF_e24vh_loose", &EF_e24vh_loose);
  }
  
  if(!b_EF_e24vh_loose0) {
    outputTree->SetBranchAddress("EF_e24vh_loose0", &EF_e24vh_loose0);
  }
  
  if(!b_EF_e24vh_loose1) {
    outputTree->SetBranchAddress("EF_e24vh_loose1", &EF_e24vh_loose1);
  }
  
  if(!b_EF_e24vh_medium1) {
    outputTree->SetBranchAddress("EF_e24vh_medium1", &EF_e24vh_medium1);
  }
  
  if(!b_EF_e24vhi_medium1) {
    outputTree->SetBranchAddress("EF_e24vhi_medium1", &EF_e24vhi_medium1);
  }
  
  if(!b_EF_e45_medium1) {
    outputTree->SetBranchAddress("EF_e45_medium1", &EF_e45_medium1);
  }
  
  if(!b_EF_e60_medium1) {
    outputTree->SetBranchAddress("EF_e60_medium1", &EF_e60_medium1);
  }
  
  if(!b_EF_mu20it_tight) {
    outputTree->SetBranchAddress("EF_mu20it_tight", &EF_mu20it_tight);
  }
  
  if(!b_EF_mu24_tight) {
    outputTree->SetBranchAddress("EF_mu24_tight", &EF_mu24_tight);
  }
  
  if(!b_EF_mu24i_tight) {
    outputTree->SetBranchAddress("EF_mu24i_tight", &EF_mu24i_tight);
  }
  
  if(!b_EF_mu36_tight) {
    outputTree->SetBranchAddress("EF_mu36_tight", &EF_mu36_tight);
  }
  
  if(!b_EF_mu40_MSonly_barrel_tight) {
    outputTree->SetBranchAddress("EF_mu40_MSonly_barrel_tight", &EF_mu40_MSonly_barrel_tight);
  }
  
  if(!b_EF_mu18_tight_mu8_EFFS) {
    outputTree->SetBranchAddress("EF_mu18_tight_mu8_EFFS", &EF_mu18_tight_mu8_EFFS);
  }
  
  if(!b_EF_2mu13) {
    outputTree->SetBranchAddress("EF_2mu13", &EF_2mu13);
  }
  
  if(!b_EF_mu24_tight_mu6_EFFS) {
    outputTree->SetBranchAddress("EF_mu24_tight_mu6_EFFS", &EF_mu24_tight_mu6_EFFS);
  }
  
  if(!b_trig_DB_SMK) {
    outputTree->SetBranchAddress("trig_DB_SMK", &trig_DB_SMK);
  }
  
  if(!b_trig_Nav_n) {
    outputTree->SetBranchAddress("trig_Nav_n", &trig_Nav_n);
  }
  
  if(!b_trig_Nav_chain_ChainId) {
    outputTree->SetBranchAddress("trig_Nav_chain_ChainId", &trig_Nav_chain_ChainId);
  }
  
  if(!b_trig_Nav_chain_RoIType) {
    outputTree->SetBranchAddress("trig_Nav_chain_RoIType", &trig_Nav_chain_RoIType);
  }
  
  if(!b_trig_Nav_chain_RoIIndex) {
    outputTree->SetBranchAddress("trig_Nav_chain_RoIIndex", &trig_Nav_chain_RoIIndex);
  }
  
  if(!b_trig_RoI_EF_e_egammaContainer_egamma_Electrons) {
    outputTree->SetBranchAddress("trig_RoI_EF_e_egammaContainer_egamma_Electrons", &trig_RoI_EF_e_egammaContainer_egamma_Electrons);
  }
  
  if(!b_trig_RoI_EF_e_egammaContainer_egamma_ElectronsStatus) {
    outputTree->SetBranchAddress("trig_RoI_EF_e_egammaContainer_egamma_ElectronsStatus", &trig_RoI_EF_e_egammaContainer_egamma_ElectronsStatus);
  }
  
  if(!b_trig_EF_el_n) {
    outputTree->SetBranchAddress("trig_EF_el_n", &trig_EF_el_n);
  }
  
  if(!b_trig_RoI_EF_mu_Muon_ROI) {
    outputTree->SetBranchAddress("trig_RoI_EF_mu_Muon_ROI", &trig_RoI_EF_mu_Muon_ROI);
  }
  
  if(!b_trig_RoI_EF_mu_TrigMuonEFInfoContainer) {
    outputTree->SetBranchAddress("trig_RoI_EF_mu_TrigMuonEFInfoContainer", &trig_RoI_EF_mu_TrigMuonEFInfoContainer);
  }
  
  if(!b_trig_RoI_EF_mu_TrigMuonEFInfoContainerStatus) {
    outputTree->SetBranchAddress("trig_RoI_EF_mu_TrigMuonEFInfoContainerStatus", &trig_RoI_EF_mu_TrigMuonEFInfoContainerStatus);
  }
  
  if(!b_trig_RoI_EF_mu_TrigMuonEFIsolationContainer) {
    outputTree->SetBranchAddress("trig_RoI_EF_mu_TrigMuonEFIsolationContainer", &trig_RoI_EF_mu_TrigMuonEFIsolationContainer);
  }
  
  if(!b_trig_RoI_EF_mu_TrigMuonEFIsolationContainerStatus) {
    outputTree->SetBranchAddress("trig_RoI_EF_mu_TrigMuonEFIsolationContainerStatus", &trig_RoI_EF_mu_TrigMuonEFIsolationContainerStatus);
  }
  
  if(!b_trig_EF_trigmuonef_track_MuonType) {
    outputTree->SetBranchAddress("trig_EF_trigmuonef_track_MuonType", &trig_EF_trigmuonef_track_MuonType);
  }
  
  if(!b_trig_RoI_L2_mu_CombinedMuonFeature) {
    outputTree->SetBranchAddress("trig_RoI_L2_mu_CombinedMuonFeature", &trig_RoI_L2_mu_CombinedMuonFeature);
  }
  
  if(!b_trig_RoI_L2_mu_CombinedMuonFeatureStatus) {
    outputTree->SetBranchAddress("trig_RoI_L2_mu_CombinedMuonFeatureStatus", &trig_RoI_L2_mu_CombinedMuonFeatureStatus);
  }
  
  if(!b_trig_RoI_L2_mu_MuonFeature) {
    outputTree->SetBranchAddress("trig_RoI_L2_mu_MuonFeature", &trig_RoI_L2_mu_MuonFeature);
  }
  
  if(!b_trig_RoI_L2_mu_Muon_ROI) {
    outputTree->SetBranchAddress("trig_RoI_L2_mu_Muon_ROI", &trig_RoI_L2_mu_Muon_ROI);
  }
  
  if(!b_trig_EF_trigmuonef_track_CB_pt) {
    outputTree->SetBranchAddress("trig_EF_trigmuonef_track_CB_pt", &trig_EF_trigmuonef_track_CB_pt);
  }
  
  if(!b_trig_EF_trigmuonef_track_CB_eta) {
    outputTree->SetBranchAddress("trig_EF_trigmuonef_track_CB_eta", &trig_EF_trigmuonef_track_CB_eta);
  }
  
  if(!b_trig_EF_trigmuonef_track_CB_phi) {
    outputTree->SetBranchAddress("trig_EF_trigmuonef_track_CB_phi", &trig_EF_trigmuonef_track_CB_phi);
  }
  
  if(!b_trig_EF_trigmuonef_track_SA_pt) {
    outputTree->SetBranchAddress("trig_EF_trigmuonef_track_SA_pt", &trig_EF_trigmuonef_track_SA_pt);
  }
  
  if(!b_trig_EF_trigmuonef_track_SA_eta) {
    outputTree->SetBranchAddress("trig_EF_trigmuonef_track_SA_eta", &trig_EF_trigmuonef_track_SA_eta);
  }
  
  if(!b_trig_EF_trigmuonef_track_SA_phi) {
    outputTree->SetBranchAddress("trig_EF_trigmuonef_track_SA_phi", &trig_EF_trigmuonef_track_SA_phi);
  }
  
  if(!b_trig_EF_trigmugirl_track_CB_pt) {
    outputTree->SetBranchAddress("trig_EF_trigmugirl_track_CB_pt", &trig_EF_trigmugirl_track_CB_pt);
  }
  
  if(!b_trig_EF_trigmugirl_track_CB_eta) {
    outputTree->SetBranchAddress("trig_EF_trigmugirl_track_CB_eta", &trig_EF_trigmugirl_track_CB_eta);
  }
  
  if(!b_trig_EF_trigmugirl_track_CB_phi) {
    outputTree->SetBranchAddress("trig_EF_trigmugirl_track_CB_phi", &trig_EF_trigmugirl_track_CB_phi);
  }
  
  if(!b_trig_EF_trigmuonef_EF_mu15) {
    outputTree->SetBranchAddress("trig_EF_trigmuonef_EF_mu15", &trig_EF_trigmuonef_EF_mu15);
  }
  
  if(!b_trig_EF_trigmuonef_EF_mu24i_tight) {
    outputTree->SetBranchAddress("trig_EF_trigmuonef_EF_mu24i_tight", &trig_EF_trigmuonef_EF_mu24i_tight);
  }
  
  if(!b_trig_EF_trigmuonef_EF_mu36_tight) {
    outputTree->SetBranchAddress("trig_EF_trigmuonef_EF_mu36_tight", &trig_EF_trigmuonef_EF_mu36_tight);
  }
  
  if(!b_trig_EF_trigmuonef_EF_mu40_MSonly_barrel_tight) {
    outputTree->SetBranchAddress("trig_EF_trigmuonef_EF_mu40_MSonly_barrel_tight", &trig_EF_trigmuonef_EF_mu40_MSonly_barrel_tight);
  }
  
  if(!b_trig_EF_el_Et) {
    outputTree->SetBranchAddress("trig_EF_el_Et", &trig_EF_el_Et);
  }
  
  if(!b_trig_EF_el_pt) {
    outputTree->SetBranchAddress("trig_EF_el_pt", &trig_EF_el_pt);
  }
  
  if(!b_trig_EF_el_m) {
    outputTree->SetBranchAddress("trig_EF_el_m", &trig_EF_el_m);
  }
  
  if(!b_trig_EF_el_eta) {
    outputTree->SetBranchAddress("trig_EF_el_eta", &trig_EF_el_eta);
  }
  
  if(!b_trig_EF_el_phi) {
    outputTree->SetBranchAddress("trig_EF_el_phi", &trig_EF_el_phi);
  }
  
  if(!b_trig_EF_el_px) {
    outputTree->SetBranchAddress("trig_EF_el_px", &trig_EF_el_px);
  }
  
  if(!b_trig_EF_el_py) {
    outputTree->SetBranchAddress("trig_EF_el_py", &trig_EF_el_py);
  }
  
  if(!b_trig_EF_el_pz) {
    outputTree->SetBranchAddress("trig_EF_el_pz", &trig_EF_el_pz);
  }
  
  if(!b_trig_EF_el_charge) {
    outputTree->SetBranchAddress("trig_EF_el_charge", &trig_EF_el_charge);
  }
  
  if(!b_trig_EF_el_author) {
    outputTree->SetBranchAddress("trig_EF_el_author", &trig_EF_el_author);
  }
  
  if(!b_trig_L2_combmuonfeature_eta) {
    outputTree->SetBranchAddress("trig_L2_combmuonfeature_eta", &trig_L2_combmuonfeature_eta);
  }
  
  if(!b_trig_L2_combmuonfeature_phi) {
    outputTree->SetBranchAddress("trig_L2_combmuonfeature_phi", &trig_L2_combmuonfeature_phi);
  }
  
  if(!b_trig_L2_muonfeature_eta) {
    outputTree->SetBranchAddress("trig_L2_muonfeature_eta", &trig_L2_muonfeature_eta);
  }
  
  if(!b_trig_L2_muonfeature_phi) {
    outputTree->SetBranchAddress("trig_L2_muonfeature_phi", &trig_L2_muonfeature_phi);
  }
  
  if(!b_trig_L1_mu_eta) {
    outputTree->SetBranchAddress("trig_L1_mu_eta", &trig_L1_mu_eta);
  }
  
  if(!b_trig_L1_mu_phi) {
    outputTree->SetBranchAddress("trig_L1_mu_phi", &trig_L1_mu_phi);
  }
  
  if(!b_trig_L1_mu_thrName) {
    outputTree->SetBranchAddress("trig_L1_mu_thrName", &trig_L1_mu_thrName);
  }
  
  if(!b_EF_e22vh_medium1) {
    outputTree->SetBranchAddress("EF_e22vh_medium1", &EF_e22vh_medium1);
  }
  
  if(!b_MET_Truth_NonInt_etx) {
    outputTree->SetBranchAddress("MET_Truth_NonInt_etx", &MET_Truth_NonInt_etx);
  }
  
  if(!b_MET_Truth_NonInt_ety) {
    outputTree->SetBranchAddress("MET_Truth_NonInt_ety", &MET_Truth_NonInt_ety);
  }
  
  if(!b_MET_Truth_NonInt_phi) {
    outputTree->SetBranchAddress("MET_Truth_NonInt_phi", &MET_Truth_NonInt_phi);
  }
  
  if(!b_MET_Truth_NonInt_et) {
    outputTree->SetBranchAddress("MET_Truth_NonInt_et", &MET_Truth_NonInt_et);
  }
  
  if(!b_MET_RefTau_phi) {
    outputTree->SetBranchAddress("MET_RefTau_phi", &MET_RefTau_phi);
  }
  
  if(!b_MET_RefTau_et) {
    outputTree->SetBranchAddress("MET_RefTau_et", &MET_RefTau_et);
  }
  
  if(!b_MET_RefTau_sumet) {
    outputTree->SetBranchAddress("MET_RefTau_sumet", &MET_RefTau_sumet);
  }
  
  if(!b_MET_SoftJets_phi) {
    outputTree->SetBranchAddress("MET_SoftJets_phi", &MET_SoftJets_phi);
  }
  
  if(!b_MET_SoftJets_et) {
    outputTree->SetBranchAddress("MET_SoftJets_et", &MET_SoftJets_et);
  }
  
  if(!b_MET_SoftJets_sumet) {
    outputTree->SetBranchAddress("MET_SoftJets_sumet", &MET_SoftJets_sumet);
  }
  
  if(!b_MET_RefMuon_phi) {
    outputTree->SetBranchAddress("MET_RefMuon_phi", &MET_RefMuon_phi);
  }
  
  if(!b_MET_RefMuon_et) {
    outputTree->SetBranchAddress("MET_RefMuon_et", &MET_RefMuon_et);
  }
  
  if(!b_MET_RefMuon_sumet) {
    outputTree->SetBranchAddress("MET_RefMuon_sumet", &MET_RefMuon_sumet);
  }
  
  if(!b_MET_CellOut_Eflow_phi) {
    outputTree->SetBranchAddress("MET_CellOut_Eflow_phi", &MET_CellOut_Eflow_phi);
  }
  
  if(!b_MET_CellOut_Eflow_et) {
    outputTree->SetBranchAddress("MET_CellOut_Eflow_et", &MET_CellOut_Eflow_et);
  }
  
  if(!b_MET_CellOut_Eflow_sumet) {
    outputTree->SetBranchAddress("MET_CellOut_Eflow_sumet", &MET_CellOut_Eflow_sumet);
  }
  
  if(!b_jet_AntiKt4LCTopo_n) {
    outputTree->SetBranchAddress("jet_AntiKt4LCTopo_n", &jet_AntiKt4LCTopo_n);
  }
  
  if(!b_jet_AntiKt4LCTopo_E) {
    outputTree->SetBranchAddress("jet_AntiKt4LCTopo_E", &jet_AntiKt4LCTopo_E);
  }
  
  if(!b_jet_AntiKt4LCTopo_pt) {
    outputTree->SetBranchAddress("jet_AntiKt4LCTopo_pt", &jet_AntiKt4LCTopo_pt);
  }
  
  if(!b_jet_AntiKt4LCTopo_m) {
    outputTree->SetBranchAddress("jet_AntiKt4LCTopo_m", &jet_AntiKt4LCTopo_m);
  }
  
  if(!b_jet_AntiKt4LCTopo_eta) {
    outputTree->SetBranchAddress("jet_AntiKt4LCTopo_eta", &jet_AntiKt4LCTopo_eta);
  }
  
  if(!b_jet_AntiKt4LCTopo_phi) {
    outputTree->SetBranchAddress("jet_AntiKt4LCTopo_phi", &jet_AntiKt4LCTopo_phi);
  }
  
  if(!b_jet_AntiKt4LCTopo_MET_n) {
    outputTree->SetBranchAddress("jet_AntiKt4LCTopo_MET_n", &jet_AntiKt4LCTopo_MET_n);
  }
  
  if(!b_jet_AntiKt4LCTopo_MET_wpx) {
    outputTree->SetBranchAddress("jet_AntiKt4LCTopo_MET_wpx", &jet_AntiKt4LCTopo_MET_wpx);
  }
  
  if(!b_jet_AntiKt4LCTopo_MET_wpy) {
    outputTree->SetBranchAddress("jet_AntiKt4LCTopo_MET_wpy", &jet_AntiKt4LCTopo_MET_wpy);
  }
  
  if(!b_jet_AntiKt4LCTopo_MET_wet) {
    outputTree->SetBranchAddress("jet_AntiKt4LCTopo_MET_wet", &jet_AntiKt4LCTopo_MET_wet);
  }
  
  if(!b_jet_AntiKt4LCTopo_MET_statusWord) {
    outputTree->SetBranchAddress("jet_AntiKt4LCTopo_MET_statusWord", &jet_AntiKt4LCTopo_MET_statusWord);
  }
  
  if(!b_jet_AntiKt4LCTopo_constscale_E) {
    outputTree->SetBranchAddress("jet_AntiKt4LCTopo_constscale_E", &jet_AntiKt4LCTopo_constscale_E);
  }
  
  if(!b_jet_AntiKt4LCTopo_constscale_pt) {
    outputTree->SetBranchAddress("jet_AntiKt4LCTopo_constscale_pt", &jet_AntiKt4LCTopo_constscale_pt);
  }
  
  if(!b_jet_AntiKt4LCTopo_constscale_m) {
    outputTree->SetBranchAddress("jet_AntiKt4LCTopo_constscale_m", &jet_AntiKt4LCTopo_constscale_m);
  }
  
  if(!b_jet_AntiKt4LCTopo_constscale_eta) {
    outputTree->SetBranchAddress("jet_AntiKt4LCTopo_constscale_eta", &jet_AntiKt4LCTopo_constscale_eta);
  }
  
  if(!b_jet_AntiKt4LCTopo_constscale_phi) {
    outputTree->SetBranchAddress("jet_AntiKt4LCTopo_constscale_phi", &jet_AntiKt4LCTopo_constscale_phi);
  }
  
  if(!b_jet_AntiKt4LCTopo_ActiveArea) {
    outputTree->SetBranchAddress("jet_AntiKt4LCTopo_ActiveArea", &jet_AntiKt4LCTopo_ActiveArea);
  }
  
  if(!b_jet_AntiKt4LCTopo_ActiveAreaPx) {
    outputTree->SetBranchAddress("jet_AntiKt4LCTopo_ActiveAreaPx", &jet_AntiKt4LCTopo_ActiveAreaPx);
  }
  
  if(!b_jet_AntiKt4LCTopo_ActiveAreaPy) {
    outputTree->SetBranchAddress("jet_AntiKt4LCTopo_ActiveAreaPy", &jet_AntiKt4LCTopo_ActiveAreaPy);
  }
  
  if(!b_jet_AntiKt4LCTopo_ActiveAreaPz) {
    outputTree->SetBranchAddress("jet_AntiKt4LCTopo_ActiveAreaPz", &jet_AntiKt4LCTopo_ActiveAreaPz);
  }
  
  if(!b_jet_AntiKt4LCTopo_ActiveAreaE) {
    outputTree->SetBranchAddress("jet_AntiKt4LCTopo_ActiveAreaE", &jet_AntiKt4LCTopo_ActiveAreaE);
  }
  
  if(!b_ph_n) {
    outputTree->SetBranchAddress("ph_n", &ph_n);
  }
  
  if(!b_ph_E) {
    outputTree->SetBranchAddress("ph_E", &ph_E);
  }
  
  if(!b_ph_Et) {
    outputTree->SetBranchAddress("ph_Et", &ph_Et);
  }
  
  if(!b_ph_pt) {
    outputTree->SetBranchAddress("ph_pt", &ph_pt);
  }
  
  if(!b_ph_m) {
    outputTree->SetBranchAddress("ph_m", &ph_m);
  }
  
  if(!b_ph_eta) {
    outputTree->SetBranchAddress("ph_eta", &ph_eta);
  }
  
  if(!b_ph_phi) {
    outputTree->SetBranchAddress("ph_phi", &ph_phi);
  }
  
  if(!b_ph_px) {
    outputTree->SetBranchAddress("ph_px", &ph_px);
  }
  
  if(!b_ph_py) {
    outputTree->SetBranchAddress("ph_py", &ph_py);
  }
  
  if(!b_ph_pz) {
    outputTree->SetBranchAddress("ph_pz", &ph_pz);
  }
  
  if(!b_ph_author) {
    outputTree->SetBranchAddress("ph_author", &ph_author);
  }
  
  if(!b_ph_isRecovered) {
    outputTree->SetBranchAddress("ph_isRecovered", &ph_isRecovered);
  }
  
  if(!b_ph_isEM) {
    outputTree->SetBranchAddress("ph_isEM", &ph_isEM);
  }
  
  if(!b_ph_isEMLoose) {
    outputTree->SetBranchAddress("ph_isEMLoose", &ph_isEMLoose);
  }
  
  if(!b_ph_isEMMedium) {
    outputTree->SetBranchAddress("ph_isEMMedium", &ph_isEMMedium);
  }
  
  if(!b_ph_isEMTight) {
    outputTree->SetBranchAddress("ph_isEMTight", &ph_isEMTight);
  }
  
  if(!b_ph_OQ) {
    outputTree->SetBranchAddress("ph_OQ", &ph_OQ);
  }
  
  if(!b_ph_convFlag) {
    outputTree->SetBranchAddress("ph_convFlag", &ph_convFlag);
  }
  
  if(!b_ph_isConv) {
    outputTree->SetBranchAddress("ph_isConv", &ph_isConv);
  }
  
  if(!b_ph_nConv) {
    outputTree->SetBranchAddress("ph_nConv", &ph_nConv);
  }
  
  if(!b_ph_reta) {
    outputTree->SetBranchAddress("ph_reta", &ph_reta);
  }
  
  if(!b_ph_rphi) {
    outputTree->SetBranchAddress("ph_rphi", &ph_rphi);
  }
  
  if(!b_ph_cl_E) {
    outputTree->SetBranchAddress("ph_cl_E", &ph_cl_E);
  }
  
  if(!b_ph_cl_pt) {
    outputTree->SetBranchAddress("ph_cl_pt", &ph_cl_pt);
  }
  
  if(!b_ph_cl_eta) {
    outputTree->SetBranchAddress("ph_cl_eta", &ph_cl_eta);
  }
  
  if(!b_ph_cl_phi) {
    outputTree->SetBranchAddress("ph_cl_phi", &ph_cl_phi);
  }
  
  if(!b_ph_tight) {
    outputTree->SetBranchAddress("ph_tight", &ph_tight);
  }
  
  if(!b_ph_looseAR) {
    outputTree->SetBranchAddress("ph_looseAR", &ph_looseAR);
  }
  
  if(!b_ph_tightAR) {
    outputTree->SetBranchAddress("ph_tightAR", &ph_tightAR);
  }
  
  if(!b_ph_MET_n) {
    outputTree->SetBranchAddress("ph_MET_n", &ph_MET_n);
  }
  
  if(!b_ph_MET_wpx) {
    outputTree->SetBranchAddress("ph_MET_wpx", &ph_MET_wpx);
  }
  
  if(!b_ph_MET_wpy) {
    outputTree->SetBranchAddress("ph_MET_wpy", &ph_MET_wpy);
  }
  
  if(!b_ph_MET_wet) {
    outputTree->SetBranchAddress("ph_MET_wet", &ph_MET_wet);
  }
  
  if(!b_ph_MET_statusWord) {
    outputTree->SetBranchAddress("ph_MET_statusWord", &ph_MET_statusWord);
  }
  
  if(!b_ph_Etcone20) {
    outputTree->SetBranchAddress("ph_Etcone20", &ph_Etcone20);
  }
  
  if(!b_ph_Etcone30) {
    outputTree->SetBranchAddress("ph_Etcone30", &ph_Etcone30);
  }
  
  if(!b_ph_Etcone40) {
    outputTree->SetBranchAddress("ph_Etcone40", &ph_Etcone40);
  }
  
  if(!b_ph_Etcone40_ED_corrected) {
    outputTree->SetBranchAddress("ph_Etcone40_ED_corrected", &ph_Etcone40_ED_corrected);
  }
  
  if(!b_ph_ED_median) {
    outputTree->SetBranchAddress("ph_ED_median", &ph_ED_median);
  }
  
  if(!b_ph_etas2) {
    outputTree->SetBranchAddress("ph_etas2", &ph_etas2);
  }
  
  if(!b_ph_etap) {
    outputTree->SetBranchAddress("ph_etap", &ph_etap);
  }
  
  if(!b_ph_topoEtcone20) {
    outputTree->SetBranchAddress("ph_topoEtcone20", &ph_topoEtcone20);
  }
  
  if(!b_ph_topoEtcone30) {
    outputTree->SetBranchAddress("ph_topoEtcone30", &ph_topoEtcone30);
  }
  
  if(!b_ph_topoEtcone40) {
    outputTree->SetBranchAddress("ph_topoEtcone40", &ph_topoEtcone40);
  }
  
  if(!b_ph_Ethad) {
    outputTree->SetBranchAddress("ph_Ethad", &ph_Ethad);
  }
  
  if(!b_ph_Ethad1) {
    outputTree->SetBranchAddress("ph_Ethad1", &ph_Ethad1);
  }
  
  if(!b_ph_E233) {
    outputTree->SetBranchAddress("ph_E233", &ph_E233);
  }
  
  if(!b_ph_E237) {
    outputTree->SetBranchAddress("ph_E237", &ph_E237);
  }
  
  if(!b_ph_E277) {
    outputTree->SetBranchAddress("ph_E277", &ph_E277);
  }
  
  if(!b_ph_weta2) {
    outputTree->SetBranchAddress("ph_weta2", &ph_weta2);
  }
  
  if(!b_ph_f1) {
    outputTree->SetBranchAddress("ph_f1", &ph_f1);
  }
  
  if(!b_ph_Emax2) {
    outputTree->SetBranchAddress("ph_Emax2", &ph_Emax2);
  }
  
  if(!b_ph_emaxs1) {
    outputTree->SetBranchAddress("ph_emaxs1", &ph_emaxs1);
  }
  
  if(!b_ph_Emins1) {
    outputTree->SetBranchAddress("ph_Emins1", &ph_Emins1);
  }
  
  if(!b_ph_fside) {
    outputTree->SetBranchAddress("ph_fside", &ph_fside);
  }
  
  if(!b_ph_deltaEs) {
    outputTree->SetBranchAddress("ph_deltaEs", &ph_deltaEs);
  }
  
  if(!b_ph_deltaEmax2) {
    outputTree->SetBranchAddress("ph_deltaEmax2", &ph_deltaEmax2);
  }
  
  if(!b_ph_wstot) {
    outputTree->SetBranchAddress("ph_wstot", &ph_wstot);
  }
  
  if(!b_ph_ws3) {
    outputTree->SetBranchAddress("ph_ws3", &ph_ws3);
  }
  
  if(!b_ph_vx_n) {
    outputTree->SetBranchAddress("ph_vx_n", &ph_vx_n);
  }
  
  if(!b_ph_vx_x) {
    outputTree->SetBranchAddress("ph_vx_x", &ph_vx_x);
  }
  
  if(!b_ph_vx_y) {
    outputTree->SetBranchAddress("ph_vx_y", &ph_vx_y);
  }
  
  if(!b_ph_vx_z) {
    outputTree->SetBranchAddress("ph_vx_z", &ph_vx_z);
  }
  
  if(!b_ph_vx_px) {
    outputTree->SetBranchAddress("ph_vx_px", &ph_vx_px);
  }
  
  if(!b_ph_vx_py) {
    outputTree->SetBranchAddress("ph_vx_py", &ph_vx_py);
  }
  
  if(!b_ph_vx_pz) {
    outputTree->SetBranchAddress("ph_vx_pz", &ph_vx_pz);
  }
  
  if(!b_ph_vx_E) {
    outputTree->SetBranchAddress("ph_vx_E", &ph_vx_E);
  }
  
  if(!b_ph_vx_m) {
    outputTree->SetBranchAddress("ph_vx_m", &ph_vx_m);
  }
  
  if(!b_ph_vx_nTracks) {
    outputTree->SetBranchAddress("ph_vx_nTracks", &ph_vx_nTracks);
  }
  
  if(!b_ph_vx_sumPt) {
    outputTree->SetBranchAddress("ph_vx_sumPt", &ph_vx_sumPt);
  }
  
  if(!b_ph_vx_convTrk_n) {
    outputTree->SetBranchAddress("ph_vx_convTrk_n", &ph_vx_convTrk_n);
  }
  
  if(!b_MET_RefGamma_et) {
    outputTree->SetBranchAddress("MET_RefGamma_et", &MET_RefGamma_et);
  }
  
  if(!b_MET_RefGamma_phi) {
    outputTree->SetBranchAddress("MET_RefGamma_phi", &MET_RefGamma_phi);
  }
  
  if(!b_el_MET_n) {
    outputTree->SetBranchAddress("el_MET_n", &el_MET_n);
  }
  
  if(!b_el_MET_wpx) {
    outputTree->SetBranchAddress("el_MET_wpx", &el_MET_wpx);
  }
  
  if(!b_el_MET_wpy) {
    outputTree->SetBranchAddress("el_MET_wpy", &el_MET_wpy);
  }
  
  if(!b_el_MET_wet) {
    outputTree->SetBranchAddress("el_MET_wet", &el_MET_wet);
  }
  
  if(!b_el_MET_statusWord) {
    outputTree->SetBranchAddress("el_MET_statusWord", &el_MET_statusWord);
  }
  
  if(!b_mu_staco_MET_n) {
    outputTree->SetBranchAddress("mu_staco_MET_n", &mu_staco_MET_n);
  }
  
  if(!b_mu_staco_MET_wpx) {
    outputTree->SetBranchAddress("mu_staco_MET_wpx", &mu_staco_MET_wpx);
  }
  
  if(!b_mu_staco_MET_wpy) {
    outputTree->SetBranchAddress("mu_staco_MET_wpy", &mu_staco_MET_wpy);
  }
  
  if(!b_mu_staco_MET_wet) {
    outputTree->SetBranchAddress("mu_staco_MET_wet", &mu_staco_MET_wet);
  }
  
  if(!b_mu_staco_MET_statusWord) {
    outputTree->SetBranchAddress("mu_staco_MET_statusWord", &mu_staco_MET_statusWord);
  }
  
  if(!b_MET_RefGamma_sumet) {
    outputTree->SetBranchAddress("MET_RefGamma_sumet", &MET_RefGamma_sumet);
  }
  
  if(!b_mu_staco_author) {
    outputTree->SetBranchAddress("mu_staco_author", &mu_staco_author);
  }
  
  if(!b_mu_staco_id_d0_exPV) {
    outputTree->SetBranchAddress("mu_staco_id_d0_exPV", &mu_staco_id_d0_exPV);
  }
  
  if(!b_mu_staco_id_z0_exPV) {
    outputTree->SetBranchAddress("mu_staco_id_z0_exPV", &mu_staco_id_z0_exPV);
  }
  
  if(!b_jet_AntiKt4TopoEM_flavor_component_ipplus_trk_n) {
    outputTree->SetBranchAddress("jet_AntiKt4TopoEM_flavor_component_ipplus_trk_n", &jet_AntiKt4TopoEM_flavor_component_ipplus_trk_n);
  }
  
  if(!b_trk_pt) {
    outputTree->SetBranchAddress("trk_pt", &trk_pt);
  }
  
  if(!b_trk_eta) {
    outputTree->SetBranchAddress("trk_eta", &trk_eta);
  }
  
  if(!b_trk_d0_wrtPV) {
    outputTree->SetBranchAddress("trk_d0_wrtPV", &trk_d0_wrtPV);
  }
  
  if(!b_trk_z0_wrtPV) {
    outputTree->SetBranchAddress("trk_z0_wrtPV", &trk_z0_wrtPV);
  }
  
  if(!b_trk_phi_wrtPV) {
    outputTree->SetBranchAddress("trk_phi_wrtPV", &trk_phi_wrtPV);
  }
  
  if(!b_trk_theta_wrtPV) {
    outputTree->SetBranchAddress("trk_theta_wrtPV", &trk_theta_wrtPV);
  }
  
  if(!b_trk_qoverp_wrtPV) {
    outputTree->SetBranchAddress("trk_qoverp_wrtPV", &trk_qoverp_wrtPV);
  }
  
  if(!b_trk_chi2) {
    outputTree->SetBranchAddress("trk_chi2", &trk_chi2);
  }
  
  if(!b_trk_ndof) {
    outputTree->SetBranchAddress("trk_ndof", &trk_ndof);
  }
  
  if(!b_trk_nBLHits) {
    outputTree->SetBranchAddress("trk_nBLHits", &trk_nBLHits);
  }
  
  if(!b_trk_nPixHits) {
    outputTree->SetBranchAddress("trk_nPixHits", &trk_nPixHits);
  }
  
  if(!b_trk_nSCTHits) {
    outputTree->SetBranchAddress("trk_nSCTHits", &trk_nSCTHits);
  }
  
  if(!b_trk_nTRTHits) {
    outputTree->SetBranchAddress("trk_nTRTHits", &trk_nTRTHits);
  }
  
  if(!b_trk_nTRTHighTHits) {
    outputTree->SetBranchAddress("trk_nTRTHighTHits", &trk_nTRTHighTHits);
  }
  
  if(!b_trk_nPixHoles) {
    outputTree->SetBranchAddress("trk_nPixHoles", &trk_nPixHoles);
  }
  
  if(!b_trk_nSCTHoles) {
    outputTree->SetBranchAddress("trk_nSCTHoles", &trk_nSCTHoles);
  }
  
  if(!b_trk_nTRTHoles) {
    outputTree->SetBranchAddress("trk_nTRTHoles", &trk_nTRTHoles);
  }
  
  if(!b_trk_nBLayerOutliers) {
    outputTree->SetBranchAddress("trk_nBLayerOutliers", &trk_nBLayerOutliers);
  }
  
  if(!b_el_Unrefittedtrack_d0) {
    outputTree->SetBranchAddress("el_Unrefittedtrack_d0", &el_Unrefittedtrack_d0);
  }
  
  if(!b_el_Unrefittedtrack_z0) {
    outputTree->SetBranchAddress("el_Unrefittedtrack_z0", &el_Unrefittedtrack_z0);
  }
  
  if(!b_el_Unrefittedtrack_phi) {
    outputTree->SetBranchAddress("el_Unrefittedtrack_phi", &el_Unrefittedtrack_phi);
  }
  
  if(!b_el_Unrefittedtrack_theta) {
    outputTree->SetBranchAddress("el_Unrefittedtrack_theta", &el_Unrefittedtrack_theta);
  }
  
  if(!b_el_Unrefittedtrack_qoverp) {
    outputTree->SetBranchAddress("el_Unrefittedtrack_qoverp", &el_Unrefittedtrack_qoverp);
  }
  
  if(!b_el_Unrefittedtrack_pt) {
    outputTree->SetBranchAddress("el_Unrefittedtrack_pt", &el_Unrefittedtrack_pt);
  }
  
  if(!b_el_Unrefittedtrack_eta) {
    outputTree->SetBranchAddress("el_Unrefittedtrack_eta", &el_Unrefittedtrack_eta);
  }
  
  if(!b_EF_g20_loose) {
    outputTree->SetBranchAddress("EF_g20_loose", &EF_g20_loose);
  }
  
  if(!b_EF_g40_loose) {
    outputTree->SetBranchAddress("EF_g40_loose", &EF_g40_loose);
  }
  
  if(!b_EF_g60_loose) {
    outputTree->SetBranchAddress("EF_g60_loose", &EF_g60_loose);
  }
  
  if(!b_EF_g80_loose) {
    outputTree->SetBranchAddress("EF_g80_loose", &EF_g80_loose);
  }
  
  if(!b_EF_g100_loose) {
    outputTree->SetBranchAddress("EF_g100_loose", &EF_g100_loose);
  }
  
  if(!b_EF_g120_loose) {
    outputTree->SetBranchAddress("EF_g120_loose", &EF_g120_loose);
  }
  
  if(!b_mc_n) {
    outputTree->SetBranchAddress("mc_n", &mc_n);
  }
  
  if(!b_mc_vx_x) {
    outputTree->SetBranchAddress("mc_vx_x", &mc_vx_x);
  }
  
  if(!b_mc_vx_y) {
    outputTree->SetBranchAddress("mc_vx_y", &mc_vx_y);
  }
  
  if(!b_mc_vx_z) {
    outputTree->SetBranchAddress("mc_vx_z", &mc_vx_z);
  }


}
