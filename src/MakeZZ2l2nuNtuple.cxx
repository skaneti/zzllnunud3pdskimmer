////////////////////////////////////////////////////////////////////////////////
// MakeZZ2l2nuNtuple.cxx
// Author: Steven Kaneti (with help from Nick Edwards)
// Created: 21 June 2013
//
//
////////////////////////////////////////////////////////////////////////////////

#include <algorithm>
#include <assert.h>
#include <cstdlib>
#include <cstdio>
#include <fstream>
#include <iostream>
#include <map>
#include <string>
#include <unistd.h>
#include <vector>
#include <getopt.h>

#include "TROOT.h"
#include "TChain.h"
#include "TFile.h"
#include "TTree.h"
#include "TMath.h"

#include "ZZllnunuD3PDSkimmer/ZZ2l2nuSkimmer.h"

// function for setting active branch status to 1
// by default, all branches are turned off in main
	
int main(int argc, char* argv[])
{
  
  const char* optstring = "dt:f:T:o:D:m";
  
  bool isMC                 = false;
  bool debug                = true;
  bool saveLumiInfo         = false;
  char* inputTTree          = (char*)"physics";
  char* inputFileName       = (char*)"";
  char* inputTChain         = (char*)"";
  char* outputFileName      = (char*)"zzskim.root";

     
  /* by default set the output directory to the current working directory 
   * this will be overwritten if the command line option --outputFileDirectory is set
   */
  char *outputFileDirectory = NULL;
  const size_t maxbuf = 4096;
  outputFileDirectory = getcwd(outputFileDirectory, maxbuf);
  
  static struct option long_options[] = 
  {
    /* name    has_arg       flag      val */
    {"debug"              , no_argument      ,      NULL,     'd'},
    {"inputTChain"        , required_argument,      NULL,     't'},
    {"inputFile"          , required_argument,      NULL,     'f'},
    {"inputTTree"         , required_argument,      NULL,     'T'},
    {"outputFileName"     , required_argument,      NULL,     'o'},
    {"outputFileDirectory", required_argument,      NULL,     'D'},
    {"isMC"               , no_argument      ,      NULL,     'm'},
    {"saveLumiInfo"       , no_argument      ,      NULL,     's'},
    { NULL                , no_argument      ,      NULL,      0 },
  };
  
  int longindex;
  int option;
  int begin =   0;
  int end   = 999;
  
  while ((option = getopt_long(argc, argv,
          optstring, long_options, &longindex)) != -1) {
      switch (option) {
          case 'd' :
            debug = true;
            break;
          case 't' :
            inputTChain = optarg;
          case 'f' :
            inputFileName = optarg;
            break;
          case 'T' :
            inputTTree = optarg;
            break;
          case 'o':
            outputFileName = optarg;
            break;
          case 'D':
            outputFileDirectory = optarg;
            break;
          case 'm':
            isMC = true;
            break;
          case 's':
            saveLumiInfo=true;
          case 0 :
            break;
          case '?' : 
            printf("Please supply a valid option to the program.  Exiting\n");
            exit(1); 
            break;
      }
  }
  
  gROOT->ProcessLine("#include <vector>");
  gROOT->LoadMacro("src/dict.cxx");
  
  char* outFileLoc = (char*)malloc((strlen(outputFileDirectory) + strlen(outputFileName) + 4)*sizeof(char));
  strcpy(outFileLoc, outputFileDirectory);
  strcat(outFileLoc, "/");
  strcat(outFileLoc, outputFileName);
  
  TFile* ofile = new TFile(outFileLoc, "Recreate");
  printf("Output File Location is set to %s\n", outFileLoc);
  
  std::cout << "Attempting to creating new TChain with tree name " << inputTTree << std::endl;
  TChain* myChain = new TChain(inputTTree); 
  
  std::cout << "Successfully created new TChain with tree name " << inputTTree << std::endl;

  // Require at least 1 argument: input text file with list of root files
  // Each line can be a comma separated list of one or more files
  std::cout << "Adding files " << inputTChain << " to TChain" << std::endl;
  myChain->Add(inputTChain);

  if (strcmp(inputFileName, "") == 0 && strcmp(inputTChain, "") == 0) {
    std::cout << "You must supply an input file name with the list of files to be executed"
              << std::endl;
    std::cout << "Exiting Skimmer" << std::endl;
    exit(1);
  } else if (strcmp(inputFileName, "") != 0 && strcmp(inputTChain, "") == 0) {
        
    std::string line;

    std::fstream inFile;
          
    inFile.open(inputFileName);
         
    if (inFile.is_open() == true) {
      std::cout << "found file " << inputFileName << std::endl;
    } else {
      std::cout << "unable to find file " << inputFileName << std::endl;
      exit(1); 
    }
 
    // Read lines in input line
    while (inFile >> line) {

      // Split each line by ","
      // Thanks to Nick Barlow!!
      for (size_t i = 0,n; i <= line.length(); i = n + 1) {
        n = line.find_first_of(',',i);
        if (n == std::string::npos) {
          n = line.length();
        }

        std::string tmp = line.substr(i,n-i);

        // Add file to chain
        std::cout << "Adding file to chain: " << tmp << std::endl;
        myChain->Add(tmp.c_str());
      }
    } // end while

    inFile.close();
        
    // end else if (!inputFileName.empty())
  } 

  // Turn off all branches initially, then use function to turn on
  // branches that we want
  //myChain->SetBranchStatus("*", 0);
  if (debug) std::cout << "DEBUG::(MakeZZ2l2nuNtuple) Turning off all branches" << std::endl;
  
  if (debug) {
    if (saveLumiInfo) std::cout << "DEBUG::(MakeZZ2l2nuNtuple) Turning off all branches" << std::endl;
  }
  Long64_t nEntries = myChain->GetEntries();
  if (debug) std::cout << "DEBUG::(MakeZZ2l2nuNtuple) nEntries = " << nEntries << std::endl;
 
  ZZ2l2nuSkimmer skimmer(myChain, ofile);
  
  skimmer.Init(myChain, isMC, saveLumiInfo);
 
  skimmer.Loop();
  
  skimmer.Terminate();

  if (outFileLoc) {
    free(outFileLoc);
  }
  // free dynamically allocated memory
  delete myChain;
  if (debug) std::cout << "DEBUG:: (MakeZZ2l2nuNtuple.cxx) Deleting TChain fChain " << std::endl;
  
  if (debug) std::cout << "DEBUG:: (MakeZZ2l2nuNtuple.cxx) Leaving Ntuple Maker" << std::endl;
  
  //indicate successful program termination
  
  return 0;

} // end main
/////////////////////////////////////////////////////////////////////////////////////////
