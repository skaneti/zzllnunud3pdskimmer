///////////////////////////////////////////////////////////////////////////////
// Custom skimmer
// Author: Steven Kaneti/Nick Edwards
//
// This class inherits from TSelector to Loop Over Events
// Code for event selection is in Process() function 
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <iomanip>
#include <fstream>
#include <algorithm>
#include <cmath>

#include "GoodRunsLists/TGoodRunsList.h"
#include "GoodRunsLists/TGoodRunsListReader.h"
#include "GoodRunsLists/TGoodRunsListWriter.h"

#include "ZZllnunuD3PDSkimmer/ZZ2l2nuSkimmer.h"

using namespace std;

ZZ2l2nuSkimmer::ZZ2l2nuSkimmer(TChain* fChain, TFile* fl)
{
  SetOutFile(fl);
  Init(fChain);
}
////////////////////////////////////////////////////////////////////////
//*********************************************************************
////////////////////////////////////////////////////////////////////////
ZZ2l2nuSkimmer::~ZZ2l2nuSkimmer() 
{

  if (debug) std::cout << "DEBUG::(ZZ2l2nuSkimmer.cxx) Calling ZZ2l2nuSkimmer" << " Destructor" << std::endl;

  if (!fChain) return;
  
  if (debug) std::cout << "DEBUG::(ZZ2l2nuSkimmer.cxx) Calling Delete on Current File" << " Destructor" << std::endl;
  delete fChain->GetCurrentFile();

  if (debug) std::cout << "DEBUG::(ZZ2l2nuSkimmer.cxx) End of ZZ2l2nuSkimmer" << " Destructor" << std::endl;

};
////////////////////////////////////////////////////////////////////////

Int_t ZZ2l2nuSkimmer::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

Long64_t ZZ2l2nuSkimmer::LoadTree(Long64_t entry)
{

   //std::cout << "calling ZZ2l2nuSkimmer::LoadTree" << std::endl;
   // Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      std::cout << "Incrementing nInputFile " << std::endl;
      nInputFile++;
      Notify();
   }
   return centry;
}
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////
//
// Init() function will set the output file and the output TTree
// and also Set the pointers of the Branches of the input TTree to 0
// and Set the Branch Addresses
void ZZ2l2nuSkimmer::Init(TChain* ch, bool isMC, bool saveLumiInfo) 
{

  if (!fChain) return;
  fChain = ch; //equivalently could do SetTChain(ch);
  fCurrent = -1;
  
  SetInptVctrBrchsZero();
  SetInptBrchAddrs();
  SetInptBrchStatus(); 
  
  m_noEvtsPass    = false;
  m_numEvtsPass   = 0;
  
  debug           = false;
  m_isMC          = isMC;
  m_saveLumiInfo  = saveLumiInfo;
  
  nInputFile = 0;
  
  if (m_isMC) {
  	std::cout << "Treating file as MC" << std::endl; 
  } else {
  	std::cout << "Treating file as DATA" << std::endl; 
  }
  if (m_saveLumiInfo) {
    std::cout << "WILL save luminosity info" << std::endl; 
  } else {
    std::cout << "WON'T save luminosity info" << std::endl; 
  }
  
  if (debug) std::cout << "DEBUG:: (ZZ2l2nuSkimmer.cxx) Calling ZZ2l2nuSkimmer::Notify" << std::endl;
  Notify();
  
  if (debug) std::cout << "DEBUG:: (ZZ2l2nuSkimmer.cxx) End of Init()" << std::endl; 
  
}
////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////
 

Bool_t ZZ2l2nuSkimmer::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   std::cout << "In function Notify() " << std::endl;
   NewTree();

   return kTRUE;
}
/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////

void ZZ2l2nuSkimmer::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}


Int_t ZZ2l2nuSkimmer::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}

////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////
void ZZ2l2nuSkimmer::NewTree() 
{

    std::cout << "In function NewTree" << std::endl;
	//Set Object Pointers
    std::cout << "In function NewTree; calling SetInptVctrBrchsZero" << std::endl;
    SetInptVctrBrchsZero();

	// Object Branch Addresses for Branches of input TTree
    std::cout << "In function NewTree; calling SetInptBrchAddrs" << std::endl;
	SetInptBrchAddrs(); 

	if (nInputFile) {
	  // Set the pointers in the output tree for branches that don't exist in the current tree
	  // to the correct variables, which are all zero
      std::cout << "In function NewTree; calling SetMissingBrchs" << std::endl;
	  SetMissingBrchs();
	} else {
	  // Make output file 
	  //outputFile = new TFile(Form("zzskim.root"), "RECREATE");
      outputFile->cd();
	  // Clone input tree. Branches that don't exist in the tree in the input file won't be copied
      std::cout << "In function NewTree; calling cloneTree" << std::endl;
	  outputTree = fChain->CloneTree(0);
	  // Manually create the branches in the output tree that don't exist in the input tree we cloned from
	  MakeMissingBrchs();
	}

	if (m_saveLumiInfo) {
	  ReadFileLumiInfo();
	}

	//nInputFile++;
}

//////////////////////////////////////////////////////////////////////////
//************************************************************************
//////////////////////////////////////////////////////////////////////////
void ZZ2l2nuSkimmer::Loop()
{
//   In a ROOT session, you can do:
//      Root > .L physics.C
//      Root > physics t
//      Root > t.GetEntry(12); // Fill t data members with entry number 12
//      Root > t.Show();       // Show values of entry 12
//      Root > t.Show(16);     // Read and show values of entry 16
//      Root > t.Loop();       // Loop on all entries
//

//     This is the loop skeleton where:
//    jentry is the global entry number in the chain
//    ientry is the entry number in the current Tree
//  Note that the argument to GetEntry must be:
//    jentry for TChain::GetEntry
//    ientry for TTree::GetEntry and TBranch::GetEntry
//
//       To read only selected branches, Insert statements like:
// METHOD1:
//    fChain->SetBranchStatus("*",0);  // disable all branches
//    fChain->SetBranchStatus("branchname",1);  // activate branchname
// METHOD2: replace line
//    fChain->GetEntry(jentry);       //read all branches
//by  b_branchname->GetEntry(ientry); //read only this branch
   if (fChain == 0) return;

   Long64_t nentries = fChain->GetEntriesFast();

   Long64_t nbytes = 0, nb = 0;
   for (Long64_t jentry = 0; jentry < nentries; jentry++) {
      Long64_t ientry = LoadTree(jentry);
      if (ientry < 0) break;
      nb = fChain->GetEntry(jentry);   nbytes += nb;
      if (jentry %2000 == 0) {
        std::cout << " nInputFile  = " << std::setw(6) << nInputFile;
	std::cout << " jentry = " << std::setw(8) << jentry;
	std::cout << " RunNumber = " << std::setw(9) << RunNumber;
	std::cout << " EventNumber = " << std::setw(10) << EventNumber;
	std::cout << " Lumi Block Number (lbn) = " <<  std::setw(10) << lbn << std::endl; 
      }
      Process(jentry);
      // if (Cut(ientry) < 0) continue;
   }
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
Bool_t ZZ2l2nuSkimmer::Process(Long64_t jentry)
{

	if (debug) std::cout << "DEBUG:: (ZZ2l2nuSkimmer.cxx) In ZZ2l2nuSkimmer Process" << std::endl;

	if (jentry < 0) {
	  if (debug) std::cout << "DEBUG:: (ZZ2l2nuSkimmer.cxx) Negative jentry (Negative Event)" << std::endl;
          return kFALSE;
	}

	//fChain->GetTree()->GetEntry(jentry); 
	//GetEntryQuick(jentry);
	//std::cout << "DEBUG:: (ZZ2l2nuSkimmer.cxx) Getting Current Entry" << std::endl;

	//std::cout << "DEBUG:: (ZZ2l2nuSkimmer.cxx) Processing Current Entry" << std::endl;

	if (jentry %2000 == 0) {
	  std::cout << " nInputFile  = " << std::setw(6) << nInputFile;
	  std::cout << " jentry = " << std::setw(8) << jentry;
	  std::cout << " RunNumber = " << std::setw(9) << RunNumber;
	  std::cout << " EventNumber = " << std::setw(10) << EventNumber;
	  std::cout << " Lumi Block Number (lbn) = " <<  std::setw(10) << lbn << std::endl; 
	} 

	bool doWrite = passEvent();

	if (doWrite) {
          m_noEvtsPass = false;
          m_numEvtsPass++;
	  if (debug) std::cout << "Writing out event information ... filling output tree" << std::endl;
	  //GetEntryFull(jentry);
	  outputTree->Fill();
	}


	if (debug) std::cout << "DEBUG:: (ZZ2l2nuSkimmer.cxx line 401) Successfully Processed Event " << EventNumber << std::endl;

	// Successfully Processed The Event
	return kTRUE;

} //end Bool_t ZZ2l2nuSkimmer::Process(Long64_t jentry)
//////////////////////////////////////////////////////////////////////////////
//**************************************************************************//
//////////////////////////////////////////////////////////////////////////////

void ZZ2l2nuSkimmer::Terminate()
{

  std::cout << "DEBUG:: (ZZ2l2nuSkimmer.cxx) In ZZ2l2nuSkimmer Terminate" << std::endl;

  if (m_noEvtsPass) {
    std::cout << "DEBUG:: (ZZ2l2nuSkimmer.cxx line 178) WARNING!! No Events Passed Selection Cuts! "
              << "Check for empty TTree" << std::endl;
  }
  
  if (m_numEvtsPass > 0) {
    std::cout << "DEBUG:: (ZZ2l2nuSkimmer.cxx line 183) Number of Events Passing Cuts is " 
              << m_numEvtsPass << std::endl;
  }
  
  if (m_saveLumiInfo) {
    MergeLumiInfo();
    std::cout << "DEBUG:: (ZZ2l2nuSkimmer.cxx) Merging Luminsity Information. " 
              << std::endl;
  }
 
  std::cout << "DEBUG::(ZZ2l2nuSkimmer.cxx) attempting to write to output TTree" << std::endl;
  outputTree->Write(outputTree->GetName(), TObject::kWriteDelete);
  std::cout << "DEBUG::(ZZ2l2nuSkimmer.cxx) closing output file" << std::endl;
  outputFile->Close();
  std::cout << "DEBUG::(ZZ2l2nuSkimmer.cxx) deleting output file" << std::endl;
  delete outputFile;
  
  std::cout << "DEBUG:: (ZZ2l2nuSkimmer.cxx) End of D3PDSkimmer Terminate()"
  	  << std::endl;
} // end void ZZ2l2nuSkimmer::Terminate() 
///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////

void ZZ2l2nuSkimmer::SlaveBegin(TChain* fChain) 
{
	if (debug) std::cout << "DEBUG:: (ZZ2l2nuSkimmer line 341) Calling ZZ2l2nuSkimmer::SlaveBegin"
		<< std::endl;
	// histogram initialization goes here

} // end virtual void ZZ2l2nuSkimmer::SlaveBegin

//////////////////////////////////////////////////
// Merge the lumiblock information from the 
// current file
//////////////////////////////////////////////////
void ZZ2l2nuSkimmer::ReadFileLumiInfo() {

    TObjString *lumiInfo =  (TObjString*) fChain->GetFile()->Get("Lumi/physics");
    if (lumiInfo) {
        m_lumiInfoVec.push_back(lumiInfo);
    } else {
        std::cout << "ERROR: No luminosity info found in file " << nInputFile 
                  << std::endl;
    }
} //end void ZZ2l2nuSkimmer::ReadFileLumiInfo()
//////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////
// Merge the lumiblock information from the files
// ran over
// This code adapted from Max Bellomo 
//////////////////////////////////////////////////
void ZZ2l2nuSkimmer::MergeLumiInfo() 
{
	
      // Feed all the XML strings to the GRL reader:
      Root::TGoodRunsListReader lbReader;
      std::vector< TObjString* >::const_iterator itr = m_lumiInfoVec.begin();
      std::vector< TObjString* >::const_iterator end = m_lumiInfoVec.end();
      for( ; itr != end; ++itr ) {
         // Check that it has the right format:
         if( ( *itr )->GetString().BeginsWith( "<?xml version=\"1.0\"?" ) &&
             ( *itr )->GetString().Contains( "DOCTYPE LumiRangeCollection" ) ) {
            // If it is, let's add it to the reader:
            lbReader.AddXMLString( ( *itr )->GetString() );
         } else {
        	 std::cerr << "ERROR: Lumi string not in the expected format: "
                     << ( *itr )->GetString() << std::endl;
         }
      }

      // Now interpret all the strings:
      if( ! lbReader.Interpret() ) {
              std::cerr <<  "Couldn't interpret the collected lumiblock information" << std::endl;
      }

      // Merge the lumiblock information:
      Root::TGoodRunsList mergedGRL = lbReader.GetMergedGoodRunsList();

      // Set some additional properties of the GRL:
      mergedGRL.SetVersion( "20" );
      mergedGRL.AddMetaData( "Description", "Lumiblock list created by MetadataTool" );

      // Turn the GRL into an XML string once more:
      Root::TGoodRunsListWriter lbWriter;
      lbWriter.SetGoodRunsList( mergedGRL );

      outputFile->cd();
      TDirectory* lumiDir = outputFile->mkdir("Lumi");
      TObjString outLumiStr(lbWriter.GetXMLString().Data());
      lumiDir->WriteTObject(&outLumiStr, "physics");

}  //end void ZZ2l2nuSkimmer::MergeLumiInfo() 
//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////
bool ZZ2l2nuSkimmer::passEvent()
{

  return true;

} // end ZZ2l2nuSkimmer::passEvent
///////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
void ZZ2l2nuSkimmer::SetTChain(TChain* inChain) 
{
  fChain = inChain;
} //end virtual void ZZllnunu::SetTChain

//////////////////////////////////////////////////////////////////////////////

void ZZ2l2nuSkimmer::SetOutFile(TFile* fl)
{
  outputFile = fl;
}

TChain* ZZ2l2nuSkimmer::getTChain()
{
  return fChain;
}
